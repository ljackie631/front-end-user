import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  ButtonModule,
  DropdownModule, InputTextareaModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  PanelModule
} from 'primeng/primeng';
import { ToastModule } from 'primeng/toast';
import { ImageUploaderModule } from '../shared/components/image-uploader/imageUploader.module';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoFormComponent } from './demo-form/demo-form.component';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';

@NgModule({
  declarations: [DemoFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DemoRoutingModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    PanelModule,
    DropdownModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    ImageUploaderModule,
    SearchBoxModule,
    ProductItemModule
  ]
})
export class DemoModule { }
