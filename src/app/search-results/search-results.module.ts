import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SearchResultsComponent } from './search-results.component';
import { TabMenuModule, PanelMenuModule, TabViewModule, DropdownModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';
import { NewProductsComponent } from './new-products/new-products.component';
import { SellingProductsComponent } from './selling-products/selling-products.component';
import { PopularProductsComponent } from './popular-products/popular-products.component';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';

@NgModule({
  entryComponents: [SearchResultsComponent],
  declarations: [
    SearchResultsComponent,
    NewProductsComponent,
    SellingProductsComponent,
    PopularProductsComponent
  ],
  imports: [
    CommonModule,
    TabMenuModule,
    PanelMenuModule,
    TabViewModule,
    FormsModule,
    PaginatorModule,
    ProductItemModule,
    DropdownModule,
    SearchBoxModule
  ],
  exports: [SearchResultsComponent]
})
export class SearchResultsModule {

}
