import { Component, OnInit } from '@angular/core';
import { Product } from '../../home/products/shared/models/product.model';
import { Products } from '../../shared/constants/products.constant';

@Component({
  selector: 'app-selling-products',
  templateUrl: './selling-products.component.html',
  styleUrls: ['./selling-products.component.scss']
})
export class SellingProductsComponent implements OnInit {

  products: Array<Product>;

  constructor() {}

  ngOnInit(): void {
    this.products = Products;
  }


}
