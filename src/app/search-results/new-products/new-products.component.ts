import { Component, OnInit } from '@angular/core';
import { Product } from '../../home/products/shared/models/product.model';
import { Products } from '../../shared/constants/products.constant';

@Component({
  selector: 'app-new-products',
  templateUrl: './new-products.component.html',
  styleUrls: ['./new-products.component.scss']
})
export class NewProductsComponent implements OnInit {

  products: Array<Product>;

  constructor() {}

  ngOnInit(): void {
    this.products = Products;
  }

}
