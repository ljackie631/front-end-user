import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TextEndPageComponent } from './text-end-page.component';

@NgModule({
  declarations: [
    TextEndPageComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    TextEndPageComponent
  ],
  providers: []
})

export class TextEndPageModule {

}
