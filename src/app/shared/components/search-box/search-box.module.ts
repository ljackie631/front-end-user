import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SearchBoxComponent } from './search-box.component';
import { DropdownModule, TabViewModule, ButtonModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [SearchBoxComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    DropdownModule,
    FormsModule,
    TabViewModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    ButtonModule
  ],
  exports: [
    SearchBoxComponent
  ],
  providers: [
  ]
})

export class SearchBoxModule {

}
