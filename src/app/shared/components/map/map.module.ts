import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../../../environments/environment';
import { GeoCodingApiService } from '../../services/geo-coding-api.service';
import { MapComponent } from './map.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/primeng';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapToken,
      libraries: ['places']
    }),
    AutoCompleteModule,
    InputTextModule
  ],
  declarations: [MapComponent],
  exports: [MapComponent],
  providers: [
    GeoCodingApiService
  ]
})

export class MapModule {

}
