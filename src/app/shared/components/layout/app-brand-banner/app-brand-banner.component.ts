import { Component, OnInit } from '@angular/core';
import { IOrderItem } from '../../../services/order.service';
import { BasePage } from '../../base/base-page';

@Component({
  selector: 'app-brand-banner',
  templateUrl: './app-brand-banner.component.html',
  styleUrls: ['./app-brand-banner.component.scss']
})
export class AppBrandBannerComponent extends BasePage implements OnInit {
  img = '/assets/demo/top-banner.jpg';
  orderItems: IOrderItem[] = [];

  ngOnInit(): void {
    this.orderService.getProductsInCart()
      .subscribe((orderItems: IOrderItem[]) => {
        this.orderItems = orderItems;
      });
  }
}
