import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InputTextareaModule, InputTextModule, ButtonModule, DialogModule } from 'primeng/primeng';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { AuthModule } from '../../../auth/auth.module';

import { AppBrandBannerComponent } from './app-brand-banner/app-brand-banner.component';
import { AppFooterComponent } from './app-footer/app.footer.component';
import { AppTopBarComponent } from './app-topbar/app.topbar.component';
import { MenuComponent } from './menu/menu.component';
import { SaleNotificationRegisterModule } from 'src/app/sale-notification-register/sale-notification-register.module';

@NgModule({
  declarations: [
    AppTopBarComponent,
    AppFooterComponent,
    AppBrandBannerComponent,
    MenuComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    InputTextareaModule,
    InputTextModule,
    ButtonModule,
    DynamicDialogModule,
    BrowserAnimationsModule,
    DialogModule,
    AuthModule,
    SaleNotificationRegisterModule
  ],
  exports: [
    AppTopBarComponent,
    AppFooterComponent,
    AppBrandBannerComponent,
    MenuComponent,
  ]
})
export class AppLayoutModule {

}
