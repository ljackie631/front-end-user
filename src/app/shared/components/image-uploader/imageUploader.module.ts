import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ImageUploaderComponent } from './imageUploader.component';
import { StringService } from '../../services/string.service';

@NgModule({
  declarations: [ImageUploaderComponent],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    ImageUploaderComponent
  ],
  providers: [
    StringService
  ]
})

export class ImageUploaderModule {

}
