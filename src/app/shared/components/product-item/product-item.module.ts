import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabViewModule, ButtonModule } from 'primeng/primeng';
import { ProgressBarModule } from 'primeng/progressbar';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductItemComponent } from './product-item.component';
import { AppStaticImageModule } from "../../pipes/app-static-image/app-static-image.module";

@NgModule({
  declarations: [ProductItemComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    TabViewModule,
    ReactiveFormsModule,
    ButtonModule,
    ProgressBarModule,
    RouterModule,
    AppStaticImageModule
  ],
  exports: [
    ProductItemComponent
  ],
  providers: [
  ]
})

export class ProductItemModule {

}
