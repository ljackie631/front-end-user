import { HostBinding, Input } from '@angular/core';
import { StringService } from '../../services/string.service';

export class BaseComponent {
  @Input()
  @HostBinding('attr.data-id')
  id = StringService.guidGenerator();

  @Input() classes = '';
  @Input() errors: string[] = [];
  @Input() style = {};
  @Input() isDisabled = false;
}
