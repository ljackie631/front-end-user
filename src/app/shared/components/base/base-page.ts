import { OnDestroy } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { GlobalService } from '../../services/global.service';
import { ServiceLocator } from '../../services/locator.service';
import { OrderService } from '../../services/order.service';
import { ValidatorService } from '../../services/validator.service';
import { MessageService } from 'primeng/api';
import { SwalDialogService } from '../../services/swal-dialog.service';

export class BasePage implements OnDestroy {

  public globalService: GlobalService;
  public messageService: MessageService;
  public swalDialogService: SwalDialogService;
  public authService: AuthService;
  protected validatorService: ValidatorService;
  protected orderService: OrderService;
  protected subscriptions: Subscription[] = [];

  constructor() {
    this.validatorService = ServiceLocator.injector.get(ValidatorService);
    this.authService = ServiceLocator.injector.get(AuthService);
    this.globalService = ServiceLocator.injector.get(GlobalService);
    this.swalDialogService = ServiceLocator.injector.get(SwalDialogService);
    this.messageService = ServiceLocator.injector.get(MessageService);
    this.orderService = ServiceLocator.injector.get(OrderService);
  }

  public getErrorMessages(form: FormGroup, ...controlNames: string[]): string[] {
    let errors: string[] = [];
    controlNames.forEach(name => {
      errors = errors.concat(this.getErrorMessagesFromControlName(form, name));
    });

    return errors;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription: Subscription) => {
      if (subscription) {
        subscription.unsubscribe();
      }
    });
  }

  protected scrollTop() {
    if (this.globalService.isPlatformBrowser()) {
      window.scrollTo(0, 0);
    }
  }

  protected markAsTouchedForAll(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.markAsTouchedForAll(control);
      } else if (control instanceof FormArray) {
        control.controls.forEach(innerFormGroup => {
          if (innerFormGroup instanceof FormGroup) {
            this.markAsTouchedForAll(innerFormGroup);
          }
        });
      }
    });
  }

  protected markAsUntouchedForAll(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsUntouched({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.markAsUntouchedForAll(control);
      } else if (control instanceof FormArray) {
        control.controls.forEach(innerFormGroup => {
          if (innerFormGroup instanceof FormGroup) {
            this.markAsUntouchedForAll(innerFormGroup);
          }
        });
      }
    });
  }

  protected markAsPristineForAll(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsPristine({onlySelf: true});
      } else if (control instanceof FormGroup) {
        this.markAsPristineForAll(control);
      } else if (control instanceof FormArray) {
        control.controls.forEach(innerFormGroup => {
          if (innerFormGroup instanceof FormGroup) {
            this.markAsPristineForAll(innerFormGroup);
          }
        });
      }
    });
  }

  protected getNumberOfFormErrors(form: FormGroup): number {
    this.markAsTouchedForAll(form);
    let count = 0;
    Object.keys(form.controls).forEach((controlKeyName: string) => {
      count += this.getErrorMessages(form, controlKeyName).length;
    });

    return count;
  }

  private getErrorMessagesFromControlName(form: FormGroup, ctrlName: string): string[] {
    const control = form.controls[ctrlName];
    return this.getSingleErrorMessages(control);
  }

  private getSingleErrorMessages(control: AbstractControl): string[] {
    let sErr: string[] = [];
    if (!control.valid && control.touched && control.errors) {
      sErr = Object.values(control.errors);
    }
    return sErr;
  }
}
