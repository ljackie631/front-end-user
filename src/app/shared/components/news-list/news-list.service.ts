import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { API } from '../../constants/api.constant';

@Injectable()
export class NewsListService {

  constructor(private _http: HttpClient) {
  }
  listHighlightNews() {
    return this._http.get(API.New.ListHighlight);
  }
}
