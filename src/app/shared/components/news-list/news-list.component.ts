import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NewsListService } from './news-list.service';
import { HTTP_CODES } from '../../constants/http-codes.constant';
import { BasePage } from '../base/base-page';


@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent extends BasePage implements OnInit {

  news: any[] = [];

  constructor(private newsService: NewsListService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    super();
  }

  ngOnInit() {
    this.getHighlight();
  }

  getHighlight() {
    const sub = this.newsService.listHighlightNews()
      .subscribe((res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.news = res.data;
        } else {
          this.router.navigate(['/']);
        }
      });
    this.subscriptions.push(sub);
  }

}
