import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { combineLatest, Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ILoggedInUser } from './auth.service';
import { API } from '../constants/api.constant';
import { HTTP_CODES } from '../constants/http-codes.constant';
import { IOrderItem, OrderService } from './order.service';
import { TokenStorage } from './token-storage.service';

export interface IResAccessToken {
  status: number;
  messages: string[];
  data: ILoggedInUser;
}

@Injectable()
export class InitializationApplicationService {
  tokenAccess = '';

  constructor(private http: HttpClient,
              private orderService: OrderService,
              private tokenStorage: TokenStorage) {
    this.tokenAccess = tokenStorage.getAccessToken();
  }

  checkAccessTokenState(): Observable<IResAccessToken> {
    return this.http.get<IResAccessToken>(API.Account.CheckAccessToken);
  }

  isExistToken(): Observable<any> {
    // if (this.tokenAccess === '') {
    //   return of(null);
    // }

    return combineLatest(
      [
        this.checkAccessTokenState(),
        this.orderService.getCartItems()
      ]
    )
      .pipe(
        tap((responses: any[]) => {
          const response = responses[0];
          if (response.status !== HTTP_CODES.SUCCESS) {
            this.tokenStorage.clear();
          }

          if (responses[1].status === HTTP_CODES.SUCCESS) {
            const orderItems: IOrderItem[] = (responses[1].data || []).map(d => {
              return {
                _id: d._id,
                product: d.product,
                quantity: d.quantity,
                shop: d.shop
              } as IOrderItem;
            });

            this.orderService.setProductsInCarts(orderItems);
          }
        })
      );
  }
}

export function validateAccessToken(initializationApplication: InitializationApplicationService) {
  return (): Promise<any> => {
    return new Promise<void>((resolve) => {
      initializationApplication.isExistToken()
        .subscribe(() => {
          resolve();
        });
    });
  };
}
