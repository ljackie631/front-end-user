import { Injectable } from '@angular/core';
import { SearchSelector } from '../constants/search-selector.constant';

@Injectable()
export class SearchSelectorService {

  static getTextByValue = (selector, value) => {
    const obj = selector.find(o => {
      return o.value === value;
    });

    if (obj && obj.text) {
      return obj.text;
    } else {
      return null;
    }
  }

  static getObjByValue = (selector, value) => {
    const obj = selector.find(o => {
      return o.value === value;
    });

    if (obj && obj.text) {
      return obj;
    } else {
      return null;
    }
  }

  static getCityByCode = (cd) => {
    return SearchSelector.Cities.find(city => {
      return city.code === cd;
    });
  }

  static getDistrictByValue = (Cities, value) => {
    return Cities.districts.find(d => {
      return d.id === value;
    });
  }

  static getTitleFromSearchQuery(searchQuery: any) {
    let title = '';

    let titleEles = [];
    if (searchQuery.design && searchQuery.design !== null) {
      titleEles.push(this.getTextByValue(SearchSelector.Designs, searchQuery.design));
    }

    if (searchQuery.floret) {
      titleEles.push(this.getTextByValue(SearchSelector.Florets, searchQuery.floret));
    }

    if (searchQuery.color) {
      titleEles.push(this.getTextByValue(SearchSelector.Colors, searchQuery.color));
    }

    if (searchQuery.specialOccasion) {
      titleEles.push(this.getTextByValue(SearchSelector.SpecialOccasions, searchQuery.specialOccasion));
    } else {
      if (searchQuery.topic) {
        titleEles.push(this.getTextByValue(SearchSelector.Topics, searchQuery.topic));
      }
    }

    if (searchQuery.city) {
      const objCity = this.getCityByCode(searchQuery.city);
      if (objCity && objCity.name) {
        const objDistrict = this.getDistrictByValue(objCity, searchQuery.district);
        if (objDistrict && objDistrict.name) {
          titleEles.push('tại', `${objDistrict.name},`, objCity.name);
        } else {
          titleEles.push('tại', objCity.name);
        }
      }
    }

    titleEles = titleEles.filter(e => e !== null);
    title = titleEles.join(' ');
    title = title.replace('hoa Hoa', 'hoa');
    return title;
  }

  static getSearchFeature(searchQuery: any) {

    let searchFeature = {
      city: null,
      district: null,
      priceRange: null
    };

    if (searchQuery.city) {
      const objCity = this.getCityByCode(searchQuery.city);
      if (objCity && objCity.name) {
        searchFeature.city = objCity.name;
        const objDistrict = this.getDistrictByValue(objCity, searchQuery.district);
        if (objDistrict && objDistrict.name) {
          searchFeature.district = objDistrict.name;
        }
      }
    }

    if (searchQuery.priceRange) {
      searchFeature.priceRange = this.getTextByValue(SearchSelector.PriceRanges, searchQuery.priceRange);
    }

    return searchFeature;
  }

  static mapDataForSearchBox(data: any) {

    const dataMapped = data;

    if (dataMapped.topic) {
      dataMapped.topic = this.getObjByValue(SearchSelector.Topics, dataMapped.topic);
    }

    if (dataMapped.specialOccasion) {
      dataMapped.specialOccasion = this.getObjByValue(SearchSelector.SpecialOccasions, dataMapped.specialOccasion);
    }

    if (dataMapped.design) {
      dataMapped.design = this.getObjByValue(SearchSelector.Designs, dataMapped.design);
    }

    if (dataMapped.floret) {
      dataMapped.floret = this.getObjByValue(SearchSelector.Florets, dataMapped.floret);
    }

    if (dataMapped.color) {
      dataMapped.color = this.getObjByValue(SearchSelector.Colors, dataMapped.color);
    }

    if (dataMapped.priceRange) {
      dataMapped.priceRange = this.getObjByValue(SearchSelector.PriceRanges, dataMapped.priceRange);
    }

    if (dataMapped.city) {
      dataMapped.city = this.getCityByCode(dataMapped.city);
      if (dataMapped.city && dataMapped.city.name && dataMapped.district) {
        dataMapped.district = this.getDistrictByValue(dataMapped.city, dataMapped.district);
      }
    }

    return dataMapped;
  }
}
