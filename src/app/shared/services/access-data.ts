export interface AccessData {
  accessToken: string;
  roles: any;
  userInfo: any;
}
