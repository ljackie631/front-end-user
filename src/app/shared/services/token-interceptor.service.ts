import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenStorage } from './token-storage.service';
import { environment } from '../../../environments/environment';


@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private tokenStorage: TokenStorage) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this._isUrlApi(request.url)) {
    request = request.clone({
      setHeaders: {
        'accesstoken': this.tokenStorage.getAccessToken()
      }
    });
    }

    return next.handle(request);
  }

  private _isUrlApi(url: string): boolean {
    return url.indexOf(environment.apiEndPoint) !== -1;
  }
}
