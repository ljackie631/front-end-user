import { Injectable } from '@angular/core';
import * as socketIO from 'socket.io-client';

import { AuthService } from './auth.service';
import { SocketEvents } from '../constants/socket.constants';
import { environment } from '../../../environments/environment';
import { MessageService } from 'primeng/components/common/messageservice';
import { GlobalService } from './global.service';

@Injectable()
export class SocketMessageService {

  socket = null;

  constructor(private _authService: AuthService,
              private _messageService: MessageService,
              public globalService: GlobalService) {
    this.initSocket();
  }

  initSocket() {
    if (this.globalService.isPlatformBrowser()) {
      this.socket = socketIO(environment.apiEndPoint);
      this.socket.on(SocketEvents.NOTIFY, (data: any) => {
        this.showNotification(data);
      });
    }
  }

  showNotification(data: any): void {
    try {
      data = JSON.parse(JSON.parse(data));
      this._messageService.add({
        severity: 'success',
        summary: data.title,
        detail: data.content || ''
      });
    } catch (e) {
      console.error(e);
    }
  }

  joinRoom(): void {
    if (this._authService.userInfo) {
      const userId = this._authService.userInfo._id;
      this.socket.emit(SocketEvents.JOIN, {userId});
    }
  }
}
