export namespace Validator {
  export namespace Code {
    export const Required = 'required';
    export const MinLength = 'minLength';
    export const MaxLength = 'maxLength';
    export const WrongPatternNumber = 'wrongPatternNumber';
    export const WrongPassword = 'wrongPassword';
    export const NotANumber = 'notANumber';
    export const NotEmail = 'notEmail';
  }

  export namespace Message {
    export const Required = 'Thông tin bắt buộc';
    export const MinLength = 'Quá ngắn. Ít nhất {value} kí tự';
    export const MaxLength = 'Quá dài. Tối đa {value} kí tự';
    export const WrongPatternNumber = 'Giá trị phải là chuỗi số';
    export const WrongPassword = 'Mật khẩu không khớp';
    export const NotANumber = 'Chỉ nhập số';
    export const NotEmail = 'Chỉ nhập Email';
  }
}
