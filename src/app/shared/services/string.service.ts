import { Injectable } from '@angular/core';
import { SearchSelector } from '../constants/search-selector.constant';
@Injectable()
export class StringService {
  public static guidGenerator(): string {
    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) || 0).toString(32).substring(1);
    };

    return `${S4()}_${S4()}_${new Date().getTime()}`;
  }

  static mapIFileTextValue(value: Array<any>) {
    return value.map(intro => {
      return  {
        link: intro,
        isDemo: false
      };
    });
  }

  static mapDataImage(value: Array<any>) {
    return value.map(image => {
      return image.link
    });
  }
  static getColorByValue(value: number): any {
    return SearchSelector.Colors.find(c => {
      return c.value === value;
    });
  }
  static getFloretByValue(value: number): any {
    return SearchSelector.Florets.find(c => {
      return c.value === value;
    });
  }
  static getDesignByValue(value: number): any {
    return SearchSelector.Designs.find(c => {
      return c.value === value;
    });
  }
  static getSpecialOccasionByValue(value: number): any {
    return SearchSelector.SpecialOccasions.find(c => {
      return c.value === value;
    });
  }
  static getTopicsByValue(value: number): any {
    return SearchSelector.TopicsForShop.find(c => {
      return c.value === value;
    });
  }
}
