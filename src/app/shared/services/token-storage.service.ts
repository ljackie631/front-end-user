import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ILoggedInUser } from './auth.service';
import { GlobalService } from './global.service';

@Injectable()
export class TokenStorage {

  constructor(private cookieService: CookieService,
              private globalService: GlobalService) {
  }

  public getAccessToken(): string {
    if (this.globalService.isPlatformBrowser()) {
      return this.cookieService.get('accessToken');
    }

    return '';
  }

  public setAccessToken(token: string): TokenStorage {
    if (this.globalService.isPlatformBrowser()) {
      this.cookieService.set('accessToken', token, 7, '/');
    }

    return this;
  }

  public setUserInfo(userInfo: any): any {
    if (userInfo && this.globalService.isPlatformBrowser()) {
      this.cookieService.set('userInfo', JSON.stringify(userInfo), 7, '/');
    }
    return this;
  }

  public getUserInfo(): ILoggedInUser | null {
    const userInfo: any = this.cookieService.get('userInfo');

    try {
      return JSON.parse(userInfo);
    } catch (e) {
      return null;
    }
  }

  /**
   * Remove tokens
   */
  public clear() {
    if (this.globalService.isPlatformBrowser()) {
      this.cookieService.delete('userInfo');
      this.cookieService.delete('accessToken');
    }
  }
}
