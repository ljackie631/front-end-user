import { Injectable } from '@angular/core';
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Validator } from './validator-message.constant';

export namespace ErrorNames {
  export const required = ['required', 'Bắt buộc'];
  export const patternUrl = ['patternUrl', 'Url sai'];
  export const patternNumber = ['patternNumber', 'Chỉ được phéo nhập số'];
  export const patternHotlineNumber = ['patternHotlineNumber', 'Chỉ được phéo nhập số và khoảng trắng'];
  export const fileNotLoad = ['fileNotLoad', 'Chưa có file'];
  export const minLength = ['minLength', 'Quá ngắn'];
  export const maxLength = ['maxLength', 'Quá dài'];
  export const minNumberValue = ['minNumberValue', 'Quá thấp'];
  export const maxNumberValue = ['maxMaxNumber', 'Quá cao'];
  export const slug = ['slug', 'Slug sai'];
  export const areStoreSelected = ['areaStore', 'Chưa chọn store nào'];
}
@Injectable()
export class ValidatorService {

  static isUndefined(value: any): boolean {
    return value === undefined;
  }

  static isNull(value: any): boolean {
    return value === null;
  }

  public checkRequired(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value || '';

      if (ValidatorService.isNull(value) ||
        ValidatorService.isUndefined(value) ||
        value === '') {
        errors[Validator.Code.Required] = Validator.Message.Required;
      }
      if  (Array.isArray(value) && value.length === 0) {
        errors[Validator.Code.Required] = Validator.Message.Required;
      }

      return errors;
    };
  }

  public checkMinLength(minLength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value || '';
      if (value.toString().length < minLength) {
        errors[Validator.Code.MinLength] = Validator.Message.MinLength.replace('{value}', minLength.toString());
      }

      return errors;
    };
  }

  public checkMaxLength(maxlength: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value || '';
      if (value.toString().length > maxlength) {
        errors[Validator.Code.MaxLength] = Validator.Message.MaxLength.replace('{value}', maxlength.toString());
      }

      return errors;
    };
  }

  public checkNumberic(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const value = control.value || '';
      if (isNaN(value.toString())) {
        errors[Validator.Code.NotANumber] = Validator.Message.NotANumber;
      }
      return errors;
    };
  }

  public checkEmail(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      // tslint:disable-next-line:max-line-length
      const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      const errors: ValidationErrors = {};
      const value = control.value || '';
      if (!regex.test(value.toString())) {
        errors[Validator.Code.NotEmail] = Validator.Message.NotEmail;
      }
      return errors;
    };
  }

  public checkConfirmPassword(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      const errors: ValidationErrors = {};
      const password = control.get('password').value;
      const confirmedPassword = control.get('confirmedPassword').value;

      if (password !== confirmedPassword) {
        errors[Validator.Code.WrongPassword] = Validator.Message.WrongPassword;
        control.get('confirmedPassword').setErrors({MatchPassword: Validator.Message.WrongPassword});
      }

      return errors;
    };
  }
}
