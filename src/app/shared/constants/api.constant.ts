import { environment } from '../../../environments/environment';

export namespace API {
  export const uploadImage = environment.serverImage + 'images';

  export namespace Profile {
    export const Register = environment.apiEndPoint + 'user/register';
    export const Login = environment.apiEndPoint + 'user/login';
    export const LoginByGoogle = environment.apiEndPoint + 'user/login-by-google';
    export const LoginByFacebook = environment.apiEndPoint + 'user/login-by-facebook';
    export const UpdateUserInfo = environment.apiEndPoint + 'user';
    export const ForgotPassword = environment.apiEndPoint + 'user/forget-password';
    export const ResetPassword = environment.apiEndPoint + 'user/reset-password';
    export const ConfirmAccountByOTP = environment.apiEndPoint + 'user/account-confirmation-by-code';
    export const ConfirmPhoneGoogleAccount = environment.apiEndPoint + 'user/confirm-phone-google-account';
    export const ConfirmPhoneFacebookAccount = environment.apiEndPoint + 'user/confirm-phone-facebook-account';
    export const RequestSesendOTP = environment.apiEndPoint + 'user/resend-otp';
    export const RegisterShop = environment.apiEndPoint + 'user/shop';
  }

  export namespace Product {
    export const NewProduct = environment.apiEndPoint + 'product';
    export const UpdateProduct = environment.apiEndPoint + 'product/{id}';
    export const DetailProduct = environment.apiEndPoint + 'product/detail/{id}';
    export const ProductInfosByIds = environment.apiEndPoint + 'product/info';
    export const ListSaleProducts = environment.apiEndPoint + 'product/sale';
    export const ListFeaturedProducts = environment.apiEndPoint + 'product/featured';
  }

  export namespace New {
    export const ListNews = environment.apiEndPoint + 'new';
    export const ListHighlight = environment.apiEndPoint + 'new/highlight';
    export const DetailNews = environment.apiEndPoint + 'new/{url}';
  }

  export namespace Home {
    export const Get = environment.apiEndPoint + 'product/home';
  }

  export namespace Shop {
    export const ShopProduct = environment.apiEndPoint + 'shop/products';
    export const UpdateStatusOfProducts = environment.apiEndPoint + 'shop/products/update-status';
    export const CheckSlugExist = environment.apiEndPoint + 'shop/check-shop-slug';
    export const RegisterShop = environment.apiEndPoint + 'shop';
    export const UpdateShop = environment.apiEndPoint + 'shop';
    export const DetailShop = environment.apiEndPoint + 'shop/detail';
    export const GetOrderList = environment.apiEndPoint + 'shop/order';
    export const UpdateOrderStatus = environment.apiEndPoint + 'order-item/status';
    export const StatisticOrder = environment.apiEndPoint + 'shop/statistic/order';
    export const StatisticMoney = environment.apiEndPoint + 'shop/statistic/money';
  }

  export namespace Search {
    export const SearchBox = environment.apiEndPoint + 'search/box';
    export const SearchContent = environment.apiEndPoint + 'search';
  }

  export namespace Address {
    export const GetListAddress = environment.apiEndPoint + 'address/delivery';
    export const AddNewAddress = environment.apiEndPoint + 'address/delivery';
    export const ChangeAddress = environment.apiEndPoint + 'address/delivery/:id';
    export const RemoveAddress = environment.apiEndPoint + 'address/:id';
  }

  export namespace Account {
    export const ConfirmAccount = environment.apiEndPoint + 'user/account-confirm';
    export const ResendConfirmAccount = environment.apiEndPoint + 'user/resend-confirm';
    export const CheckAccessToken = environment.apiEndPoint + 'user/info';
  }

  export namespace Order {
    export const AddProductToCart = environment.apiEndPoint + 'order';
    export const ListOrder = environment.apiEndPoint + 'order';
    export const GetItemsOrder = environment.apiEndPoint + 'order/{id}/item';
    export const SubmitOrder = environment.apiEndPoint + 'order';
    export const SubmitOrderForGuest = environment.apiEndPoint + 'order/no-login';
    export const PendingOrder = environment.apiEndPoint + 'order/pending';
    export const DeleteOrderItem = environment.apiEndPoint + 'order/item/{id}';
    export const UpdateOrderItem = environment.apiEndPoint + 'order-item/new/{id}';
    export const GetOrderDetail = environment.apiEndPoint + 'order/:orderId/item';
    export const AddManyProduct = environment.apiEndPoint + 'order/many';
    export const LookupOrder = environment.apiEndPoint + 'order/guest-detail/:code';
    export const GetShippingCost = environment.apiEndPoint + 'order/shipping-cost';
    export const GetNoLoginShippingCost = environment.apiEndPoint + 'order/no-login-shipping-cost';
  }

  export namespace Notify {
    export const GetNotify = environment.apiEndPoint + 'notify';
  }

  export namespace SaleNotification {
    export const registerSaleNotification  = environment.apiEndPoint + 'sale-notification/register';
  }
}
