import { NgModule } from '@angular/core';
import { LoginCanActivate } from './login';
import { ShopCanActivate } from './shop';
import { CartCanActive } from './cart';

@NgModule({
  providers: [ShopCanActivate, LoginCanActivate, CartCanActive]
})
export class GuardModule {

}
