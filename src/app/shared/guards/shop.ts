import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { GlobalConstant } from '../constants/global.constant';
import { AuthService } from '../services/auth.service';
import UserType = GlobalConstant.UserType;

@Injectable()
export class ShopCanActivate implements CanActivate {
  constructor(private router: Router,
              private authService: AuthService) {

  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authService.userInfo.type !== UserType.UserAsShop) {
      this.router.navigate(['/trang-ca-nhan']);
      return false;
    }

    return true;
  }
}
