import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FeaturedProductsComponent } from './featured-products.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      pathMatch: 'full',
      component: FeaturedProductsComponent
    }
  ])],
  exports: [RouterModule]
})
export class FeaturedProductsRoutingModule {

}
