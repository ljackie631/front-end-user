import { Injectable } from '@angular/core';

import { API } from '../shared/constants/api.constant';
import { HttpClient } from '@angular/common/http';
import { Product } from '../detail-product/shared/models/Product.model';


export interface IResListProducts {
  status: number;
  messages: string[];
  data: {
    meta: {
      totalItems?: number;
      limit?: number;
      page?: number;
    };
    products?: Product[];
    relatedProducts?: Product[];
    shopInfo?: any;
    totalItems?: number;
    searchQuery?: any;
  };
}
@Injectable({
  providedIn: 'root'
})
export class FeaturedProductsService {

  constructor(private _http: HttpClient) {
  }

  listFeaturedroducts(params: any) {
    return this._http.get(API.Product.ListFeaturedProducts, {params});
  }
}
