import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SearchSelector } from '../shared/constants/search-selector.constant';
import { ProfileService } from '../profile/profile.service';
import { SeoService } from '../shared/services/seo.service';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { IResShop } from '../profile/register-shop/register-shop.component';
import { BasePage } from '../shared/components/base/base-page';
import urlSlug from 'url-slug';
import { GlobalConstant } from '../shared/constants/global.constant';
import { RegisterShopService } from './register-shop.service';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';
import { IMap } from '../shared/components/map/map.component';

export interface IBody {
  name: string;
  shopName: string;
  slug: string;
  images: string[];
  phone: string;
  email: string;
  availableShipCountry: boolean;
  availableShipAddresses?: {
    city: string,
    district?: number
  }[];
  address: string;
  longitude: number;
  latitude: number;
}

export interface IResShop {
  status: number;
  messages: string[];
  data: {};
}

interface ILocation {
  district: any;
  selectedCityCode: string;
  selectedDistrict: number[];
  isValid: boolean;
  isDuplicateCity: boolean;
  isSelectAllDistrict: boolean;
}

interface ICollectionLocation {
  amountOfError: number;
  locations: ILocation[];
}


@Component({
  selector: 'app-register-shop',
  templateUrl: './register-shop.component.html',
  styleUrls: ['./register-shop.component.scss']
})
export class RegisterShopComponent  extends BasePage implements OnInit {

  form: FormGroup;
  showOTPPopup = false;
  iconInputResendOTP = 'pi pi-replay';
  otpInput = '';
  genderRadio = GlobalConstant.Gender;
  deliveryFlag = false;
  slugFlag = {
    display: false,
    state: false
  };
  registerPhone = '';
  collectionLocation: ICollectionLocation = {amountOfError: 0, locations: []};
  citySource = SearchSelector.Cities;
  _location = {
    latitude: 10.8230989,
    longitude: 106.6296638
  };

  constructor(private fb: FormBuilder,
              private profileService: ProfileService,
              private registerShopService: RegisterShopService,
              private _authService: AuthService,
              private router: Router,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this.initForm();
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Đăng ký bán hàng',
        routerLink: '/dang-ky-ban-hang',
        icon: 'pi pi-th-large'
      }
    ]);
    this.initLocation();
    this.seoService.setHeader('Đăng ký bán hàng');
  }

  private initForm() {
    this.form = this.fb.group({
      shopName: ['', [
        this.validatorService.checkRequired()
      ]],
      slug: ['', [
        this.validatorService.checkRequired()
      ]],
      images: [[], [
        this.validatorService.checkRequired()
      ]],
      address: [[], [
        this.validatorService.checkRequired()
      ]],
      email: ['', [
        this.validatorService.checkRequired(),
        this.validatorService.checkEmail()
      ]],
      name: ['', [this.validatorService.checkRequired()]],
      phone: ['', [this.validatorService.checkRequired(), this.validatorService.checkNumberic()]],
      password: ['', [this.validatorService.checkRequired()]],
      confirmedPassword: ['', [this.validatorService.checkRequired()]],
      gender: [this.genderRadio.GENDER_MALE, this.validatorService.checkRequired()]
    }, {validators: this.validatorService.checkConfirmPassword()});
  }
  onChangeCity(option: any, index: number) {
    const isDuplicateCity = this.collectionLocation.locations
      .filter(city => city.selectedCityCode === option.value.code).length;
    if (isDuplicateCity > 0) {
      this.collectionLocation.locations[index].isDuplicateCity = false;
      this.collectionLocation.locations[index].selectedCityCode = null;
      this.collectionLocation.locations[index].selectedDistrict = [];
      this.collectionLocation.locations[index].district = null;
    } else {
      this.collectionLocation.locations[index].isDuplicateCity = true;
      this.collectionLocation.locations[index].isValid = true;
      this.collectionLocation.locations[index].isSelectAllDistrict = true;
      this.collectionLocation.locations[index].selectedCityCode = option.value.code;
      this.collectionLocation.locations[index].selectedDistrict = option.value.districts.map(d => d.id);
      this.collectionLocation.locations[index].district = option.value.districts;
    }
  }

  onChangeSwitch(event: any) {
    this.deliveryFlag = event.checked;
  }

  onSubmit() {
    const countError = this.getNumberOfFormErrors(this.form);
    this.validateCollectionLocation();
    if (countError === 0 && this.slugFlag.state && this.collectionLocation.amountOfError === 0) {
      const body = this.exchangeFormToBody();
      this.registerPhone = body.phone;
      this.registerShop(body);
    }
  }

  checkSlug(event: any) {
    if (event.target.value === '') {
      this.slugFlag.display = false;
      this.slugFlag.state = false;
    } else {
      const sub = this.profileService.checkSlugExist(event.target.value)
        .subscribe((response: IResShop) => {
          if (response.status === HTTP_CODES.SUCCESS) {
            this.slugFlag.display = true;
            this.slugFlag.state = true;
          } else {
            this.slugFlag.display = true;
            this.slugFlag.state = false;
          }
        });
      this.subscriptions.push(sub);
    }
  }

  registerShop(body: IBody) {
    const sub = this.registerShopService.registerShop(body)
      .subscribe((response: IResShop) => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.messageService.add({
            severity: 'success',
            summary: 'Thông Báo',
            detail: 'Đăng ký bán hàng thành công',
            life: 10000
          });
          this.showOTPPopup = true;
        } else {
          let errorMessage = '';
          response.messages.forEach(event => {
            errorMessage += event + '.';
          });
          this.messageService.add({severity: 'error', summary: 'Thông Báo', detail: errorMessage, life: 10000});
        }
      });
    this.subscriptions.push(sub);
  }

  exchangeFormToBody(): IBody {
    const images = [];
    this.form.value.images.forEach(x => {
      images.push(x.link);
    });
    const body = {
      shopName: this.form.value.shopName,
      slug: this.form.value.slug,
      images,
      availableShipCountry: this.deliveryFlag,
      availableShipAddresses: [],
      address: this.form.value.address,
      name: this.form.value.name,
      email: this.form.value.email,
      phone: this.form.value.phone,
      password: this.form.value.password,
      confirmedPassword: this.form.value.confirmedPassword,
      gender: this.form.value.gender,
      latitude: this._location.latitude,
      longitude: this._location.latitude
    };
    if (!this.deliveryFlag) {
      this.collectionLocation.locations.forEach(city => {
        city.selectedDistrict.forEach(districtId => {
          body.availableShipAddresses.push({city: city.selectedCityCode, district: districtId});
        });
      });
      return body;
    }
    return body;
  }

  initLocation() {
    this.addLocation();
  }

  addLocation() {
    const location = {
      district: null,
      selectedCityCode: null,
      selectedDistrict: [],
      isValid: true,
      isDuplicateCity: true,
      isSelectAllDistrict: true
    };
    this.collectionLocation.locations.push(location);
  }

  removeLocation(index: number) {
    this.collectionLocation.locations.splice(index, 1);
  }

  private validateCollectionLocation() {
    if (!this.deliveryFlag) {
      this.collectionLocation.amountOfError = this.collectionLocation.locations
        .map(element => {
          if (element.selectedCityCode === null || element.selectedDistrict.length === 0) {
            element.isValid = false;
          }
          return element.selectedCityCode;
        })
        .filter((cityStr: string) => cityStr === null)
        .length;
    } else {
      this.collectionLocation.amountOfError = 0;
    }
  }

  onNameChanged(event: any) {
    this.form.controls.slug.setValue(urlSlug(event.target.value));
    this.checkSlug(event);
  }

  chooseDistricts(index: number) {
    const existDistrict = this.collectionLocation.locations[index].selectedDistrict.length;
    const amountOfDistricts = this.collectionLocation.locations[index].district.length;
    this.collectionLocation.locations[index].isSelectAllDistrict = existDistrict >= amountOfDistricts;
  }

  selectAllDistrictsOfCity(event: any, locationIndex: number) {
    if (event) {
      this.collectionLocation.locations[locationIndex].selectedDistrict = this.collectionLocation.locations[locationIndex].district
        .map(d => d.id);
      this.collectionLocation.locations[locationIndex].isSelectAllDistrict = true;
    } else {
      this.collectionLocation.locations[locationIndex].selectedDistrict = [];
      this.collectionLocation.locations[locationIndex].isSelectAllDistrict = false;
    }
  }


  confirmAccountByOTPCode() {
    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.confirmAccountByOTPCode(this.registerPhone, this.otpInput)
      .subscribe((res: any) => {
        this.globalService.setLoadingSpinner(false);
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
          this.swalDialogService.openSuccess(GlobalConstant.Messages.WaitingForApprovedMessages);
          this._authService.saveUserInto(res.data.entries[0]);
          this._authService.saveAccessToken(res.data.meta.token);
          this.router.navigate(['/']);
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
      });

    this.subscriptions.push(sub);
  }

  onMapChanged(value: IMap | null) {
    if (value) {
      this._location = {
        longitude: value.longitude,
        latitude: value.latitude
      };
      this.form.controls.address.setValue(value.address);
      this.form.controls.longitude.setValue(value.longitude);
      this.form.controls.latitude.setValue(value.latitude);
    }
  }

  resendOTPCode() {
    this.globalService.setLoadingSpinner(true);
    const sub = this._authService.requestResendOTPCode(this.registerPhone)
      .subscribe((res: any) => {
        this.globalService.setLoadingSpinner(false);
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
      });

    this.subscriptions.push(sub);
  }

}
