import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { RegisterShopComponent } from './register-shop.component';
import { RegisterShopRoutingModule } from './register-shop-routing.module';
import { CardModule } from 'primeng/card';
import {
  ButtonModule, CalendarModule,
  CheckboxModule,
  ChipsModule, ConfirmationService, ConfirmDialogModule, DialogModule, DropdownModule, EditorModule,
  InputMaskModule, InputSwitchModule,
  InputTextareaModule, InputTextModule, KeyFilterModule,
  MessageModule,
  MessagesModule, PaginatorModule, PasswordModule, RadioButtonModule, TabViewModule, ToolbarModule
} from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ImageUploaderModule } from '../shared/components/image-uploader/imageUploader.module';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { MapModule } from '../shared/components/map/map.module';
import { RegisterShopService } from './register-shop.service';
import { DynamicDialogModule } from 'primeng/dynamicdialog';

@NgModule({
  declarations: [RegisterShopComponent],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RegisterShopRoutingModule,
    MessagesModule,
    MessageModule,
    InputMaskModule,
    InputTextareaModule,
    ChipsModule,
    ButtonModule,
    ImageUploaderModule,
    CardModule,
    InputTextModule,
    DropdownModule,
    ButtonModule,
    TableModule,
    ToolbarModule,
    TabViewModule,
    PaginatorModule,
    ToolbarModule,
    DialogModule,
    RadioButtonModule,
    CalendarModule,
    PasswordModule,
    CheckboxModule,
    ToastModule,
    ConfirmDialogModule,
    DynamicDialogModule,
    InputSwitchModule,
    EditorModule,
    KeyFilterModule,
    MapModule
  ],
  providers: [
    RegisterShopService
  ],
  exports: []
})
export class RegisterShopModule { }
