import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { RegisterShopComponent } from './register-shop.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      component: RegisterShopComponent,
      children: []
    }
  ])],
  exports: [RouterModule]
})
export class RegisterShopRoutingModule { }
