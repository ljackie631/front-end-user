import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleProductsComponent } from './sale-products.component';
import { SaleProductsRoutingModule } from './sale-products-routing.module';
import { SaleProductsService } from './sale-products.service';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { CardModule, DropdownModule, PanelMenuModule, TabMenuModule, TabViewModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { PaginatorModule } from 'primeng/paginator';

@NgModule({
  declarations: [SaleProductsComponent],
  imports: [
    CommonModule,
    SaleProductsRoutingModule,
    CommonModule,
    TabMenuModule,
    PanelMenuModule,
    TabViewModule,
    FormsModule,
    PaginatorModule,
    ProductItemModule,
    DropdownModule,
    SearchBoxModule,
    CardModule
  ],
  providers: [
      SaleProductsService
  ]
})
export class SaleProductsModule { }
