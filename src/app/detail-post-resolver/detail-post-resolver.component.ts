import { HttpClient } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ViewChild, Type } from '@angular/core';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { NavigationEnd, Router, ActivatedRoute, Params } from '@angular/router';
import { SearchResultsComponent } from '../search-results/search-results.component';
import { BasePage } from '../shared/components/base/base-page';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { IResSearchContent, SearchService } from '../shared/services/search.service';
import { IPost } from './IPost.interface';
import { PostItem } from './post-item';
import { PostDirective } from './post.directive';
import { DetailProductComponent } from '../detail-product/detail-product.component';

@Component({
  selector: 'app-detail-post-resolver',
  templateUrl: './detail-post-resolver.component.html'
})

export class DetailPostResolverComponent extends BasePage {
  private currentQueryParams: Params;
  private currentUrl = '';
  private availableComponents = {
    productList: new PostItem(SearchResultsComponent, null),
    productDetail: new PostItem(DetailProductComponent, null)
  };
  dataStateKey = makeStateKey('data');

  @ViewChild(PostDirective) postHost: PostDirective;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver,
              private _searchService: SearchService) {
    super();
    const sub = router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        let url = event.urlAfterRedirects.split('?')[0];
        if (url && url[0] === '/') {
          url = url.slice(1);
          this.currentUrl = url;
        }

        this.callApiToDetectType(url);
      }
    });

    const sub1 = this.activatedRoute.queryParams.subscribe((params: Params) => {
      this.currentQueryParams = params;
    });

    this.subscriptions.push(sub);
    this.subscriptions.push(sub1);
  }

  callApiToDetectType(url: string) {
    this._searchService.searchContent({url, limit: 12, page: 1})
      .subscribe((res: IResSearchContent) => {
          if (res.status === HTTP_CODES.SUCCESS) {
            this.loadComponent(res);
          }
        }
      );
  }

  loadTypeComponent(data: { isList: boolean, isDetail: boolean }): Type<any> | null {
    if (data.isList) {
      return this.availableComponents.productList.component;
    }

    if (data.isDetail) {
      return this.availableComponents.productDetail.component;
    }

    return null;
  }

  getUrlAndCustomUrl(data: any): { url: string; customUrl: string } {
    const result = {
      url: '',
      customUrl: ''
    };

    if (data.isList) {
      result.url = data.seo.url;
      result.customUrl = data.seo.customUrl;
    } else {
      result.url = data.data.url;
      result.customUrl = data.data.customUrl;
    }

    return result;
  }

  getUrlToBeRedirect(urlConfig: { url: string; customUrl: string }): string {
    const eleStrs = this.currentUrl.split('/');
    return `${eleStrs[0]}/${urlConfig.customUrl}`;
  }

  loadComponent(res: IResSearchContent) {
    const component: Type<any> | null = this.loadTypeComponent({isDetail: res.data.isDetail, isList: res.data.isList});
    if (component === null) {
      return;
    }

    // Each component must have property "data"
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

    const viewContainerRef = this.postHost.viewContainerRef;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent(componentFactory);
    (componentRef.instance as IPost).data = res;
  }
}
