import { Pipe, PipeTransform } from '@angular/core';
import { SearchSelector } from '../../../shared/constants/search-selector.constant';
import Cities = SearchSelector.Cities;

@Pipe({
  name: 'city'
})
export class CityPipe implements PipeTransform {
  transform(code: any, arg?: any): any {
    const cities = Cities;
    for (const city of cities) {
      if (city.code === code) {
        return city.name;
      }
    }
  }
}
