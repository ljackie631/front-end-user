import { Pipe, PipeTransform } from '@angular/core';
import { SearchSelector } from '../../../shared/constants/search-selector.constant';
import Cities = SearchSelector.Cities;

@Pipe({
  name: 'ward'
})
export class WardPipe implements PipeTransform {
  transform(id: number, arg?: any): any {
    let districts: any[] = [];
    const cities = Cities;
    districts = cities.map(city => city.districts);
    for (const district of districts) {
      for (const items of district) {
        for (const item of items.wards) {
          if (item.id === id) {
            return item.name;
          }
        }
      }
    }
  }
}
