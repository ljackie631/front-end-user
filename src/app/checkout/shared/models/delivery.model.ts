export interface IDeliveryInfo {
  address: string;
  city: string;
  deliveryTime: Date;
  expectedDeliveryTime: string;
  district: number;
  name: string;
  phone: string;
  user: string;
  ward: number;
  buyerName?: string;
  buyerPhone?: string;
  buyerEmail?: string;
  email?: string;
  longitude?: number;
  latitude?: number;
  _id?: string;
  contentOrder?: string;
  totalShippingCost: number;
  note: string;
}
