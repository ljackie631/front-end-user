import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { IMap } from '../../shared/components/map/map.component';
import { GlobalService } from '../../shared/services/global.service';
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { BasePage } from '../../shared/components/base/base-page';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { Router } from '@angular/router';
import { IDeliveryInfo } from '../shared/models/delivery.model';

export interface IDelivery {
  _id: string;
  address: string;
  city: string;
  district: number;
  name: string;
  phone: string;
  user: string;
  ward: number;
}

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss'],
  providers: [MessageService]
})
export class DeliveryComponent extends BasePage implements OnInit {
  // districts: Array<any> = [];
  // wards: Array<any> = [];
  deliveryForm: FormGroup;
  buyerInfoForm: FormGroup;
  addressForm: FormGroup;
  deliveries: IDelivery[];
  items: MenuItem[];
  date: Date;
  time: Date;
  minDate: Date = new Date(Date.now());
  info: any;
  expectedDeliveryTimes = [
    {label: 'Trong ngày', value: 'Trong ngày'},
    {label: '6h -> 7h', value: '6h -> 7h'},
    {label: '7h -> 8h', value: '7h -> 8h'},
    {label: '8h -> 9h', value: '8h -> 9h'},
    {label: '9h -> 10h', value: '9h -> 10h'},
    {label: '10h -> 11h', value: '10h -> 11h'},
    {label: '11h -> 12h', value: '11h -> 12h'},
    {label: '12h -> 13h', value: '12h -> 13h'},
    {label: '13h -> 14h', value: '13h -> 14h'},
    {label: '14h -> 15h', value: '14h -> 15h'},
    {label: '15h -> 16h', value: '15h -> 16h'},
    {label: '16h -> 17h', value: '16h -> 17h'},
    {label: '17h -> 18h', value: '17h -> 18h'},
    {label: '18h -> 19h', value: '18h -> 19h'},
    {label: '19h -> 20h', value: '19h -> 20h'},
    {label: '20h -> 21h', value: '20h -> 21h'}
  ];
  buyerIsReceiver = false;
  contentOrder = '';
  note = '';

  orderItems: any;

  constructor(public globalService: GlobalService,
              private _fb: FormBuilder,
              private _router: Router) {
    super();
  }

  ngOnInit() {
    this.scrollTop();
    this.getDelivery();
    this._initBreadCrumb();
    this.initForm();
    this.getDeliveryDateTime();

    this.getCurrentItemsOrder();

    const sub = this.orderService.getProductsInCart()
      .subscribe((items: any[]) => {
        if (items.length === 0) {
          alert('Giỏ hàng trống');
          this._router.navigate(['/']);
        }
      });
    this.subscriptions.push(sub);
    this.items = [
      {label: 'Giao hàng'},
      {label: 'Thanh toán'},
      {label: 'Hoàn tất'}
    ];
  }

  getCurrentItemsOrder() {
    const sub = this.orderService.getProductsInCart()
      .subscribe(data => {
        this.orderItems = data;
      });

    this.subscriptions.push(sub);
  }

  initForm() {
    if (this.authService.userInfo) {
      this.deliveryForm = this._fb.group({
        date: [new Date(), [this.validatorService.checkRequired()]],
        expectedDeliveryTime: ['', [this.validatorService.checkRequired()]]
      });

      this.addressForm = this._fb.group({
        name: ['', [this.validatorService.checkRequired()]],
        phone: ['', [
          this.validatorService.checkRequired(),
          this.validatorService.checkNumberic(),
          this.validatorService.checkMinLength(10),
          this.validatorService.checkMaxLength(11)
        ]],
        address: ['', [this.validatorService.checkRequired()]],
        longitude: [null, [this.validatorService.checkRequired()]],
        latitude: [null, [this.validatorService.checkRequired()]]
      });
    } else {
      this.buyerInfoForm = this._fb.group({
        name: ['', [this.validatorService.checkRequired()]],
        phone: ['', [
          this.validatorService.checkRequired(),
          this.validatorService.checkNumberic(),
          this.validatorService.checkMinLength(10),
          this.validatorService.checkMaxLength(11)
        ]],
        email: ['', [this.validatorService.checkRequired(), this.validatorService.checkEmail()]]
      });
      this.deliveryForm = this._fb.group({
        name: ['', [this.validatorService.checkRequired()]],
        phone: ['', [
          this.validatorService.checkRequired(),
          this.validatorService.checkNumberic(),
          this.validatorService.checkMinLength(10),
          this.validatorService.checkMaxLength(11)
        ]],
        address: ['', [this.validatorService.checkRequired()]],
        date: [new Date(), [this.validatorService.checkRequired()]],
        expectedDeliveryTime: ['', [this.validatorService.checkRequired()]],
        longitude: [null, [this.validatorService.checkRequired()]],
        latitude: [null, [this.validatorService.checkRequired()]]
      });
    }
  }

  getDeliveryDateTime() {
    const sub = this.orderService.getDeliveryInfo().subscribe(
      (res: IDeliveryInfo) => {
        this.date = this.time = res.deliveryTime ? new Date(res.deliveryTime) : new Date(Date.now());
      });
    this.subscriptions.push(sub);
  }

  onChangeDate() {
    this.deliveryForm.controls.date.setValue(this.date);
    this.deliveryForm.controls.date.markAsTouched();
  }

  onSubmitAddress() {
    const countErrors = this.getNumberOfFormErrors(this.addressForm);
    if (countErrors > 0) {
      this.swalDialogService.openWarning('Vui lòng kiểm tra lại thông tin địa chỉ mới');
      return;
    }

    const formValue = {...this.addressForm.value};
    this.globalService.setLoadingSpinner(true);

    const sub = this.orderService.addDelivery(formValue).subscribe(
      (res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess('Thêm địa chỉ thành công!');
          this.getDelivery();
          this.addressForm.reset();
          this.scrollTop();
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }

        this.globalService.setLoadingSpinner(false);
      }
    );

    this.subscriptions.push(sub);
    this.deliveryForm.reset();
  }

  getDelivery() {
    const sub = this.orderService.getAllDeliveries().subscribe(
      (res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.deliveries = res.data.entries;
        }
      }
    );

    this.subscriptions.push(sub);
  }

  onPayment(id: string) {
    const countErrors = this.getNumberOfFormErrors(this.deliveryForm);
    if (countErrors > 0) {
      this.swalDialogService.openWarning('Thông tin nhận người và địa chỉ nhận hàng là bắt buộc');
      return;
    }

    const address = this.deliveries.filter(delivery => delivery._id === id);
    const data = {
      ...this.deliveryForm.value,
      ...address[0],
      contentOrder: this.contentOrder,
      note: this.note,
      totalShippingCost: 0
    };
    this.globalService.setLoadingSpinner(true);


    this.getShippingCostAndGoToPayment(data);
  }

  getShippingCostAndGoToPayment(data) {
    const sub = this.orderService.getShippingCost(data._id)
      .subscribe(res => {
        data.totalShippingCost = res.data.totalShippingCost;
        this.orderService.setDeliveryInfo(data);
        this._router.navigate(['/mua/thanh-toan']);
      });
    this.subscriptions.push(sub);
  }

  getNoLoginShippingCostAndGoToPayment(data) {
    const addressInfo = {
      address: data.address,
      longitude: data.longitude,
      latitude: data.latitude
    };
    const items = this.orderItems.map(io => {
      return {
        productId: io.product._id
      };
    });
    const sub = this.orderService.getNoLoginShippingCost(addressInfo, items)
      .subscribe(res => {
        data.totalShippingCost = res.data.totalShippingCost;
        this.orderService.setDeliveryInfo(data);
        this._router.navigate(['/mua/thanh-toan']);
      });
    this.subscriptions.push(sub);
  }

  onPaymentNoLogin() {
    const countDeliveryErrors = this.getNumberOfFormErrors(this.deliveryForm);
    const countBuyerFormErrors = this.getNumberOfFormErrors(this.buyerInfoForm);
    const countErrors = countDeliveryErrors + countBuyerFormErrors;
    if (countErrors > 0) {
      return this.swalDialogService.openWarning('Thông tin nhận người và địa chỉ nhận hàng là bắt buộc');
    }

    const buyerInfo: any = {
      name: this.buyerIsReceiver ? this.deliveryForm.value.name : this.buyerInfoForm.value.name,
      phone: this.buyerIsReceiver ? this.deliveryForm.value.phone : this.buyerInfoForm.value.phone,
      email: this.buyerIsReceiver ? '' : this.buyerInfoForm.value.email
    };

    const data = {
      ...this.deliveryForm.value,
      deliveryTime: this.deliveryForm.value.date,
      buyerName: buyerInfo.name,
      buyerPhone: buyerInfo.phone,
      buyerEmail: buyerInfo.email,
      contentOrder: this.contentOrder,
      totalShippingCost: 0,
      note: this.note
    };

    this.orderService.setDeliveryInfo(data);
    this.globalService.setLoadingSpinner(true);
    this.getNoLoginShippingCostAndGoToPayment(data);
  }

  toEdit($element) {
    $element.scrollIntoView({behavior: 'smooth', block: 'start', inline: 'nearest'});
  }

  onMapChanged(value: IMap | null) {
    if (value) {
      if (this.authService.userInfo) {
        this.addressForm.controls.longitude.setValue(value.longitude);
        this.addressForm.controls.latitude.setValue(value.latitude);
      } else {
        this.deliveryForm.controls.longitude.setValue(value.longitude);
        this.deliveryForm.controls.latitude.setValue(value.latitude);
      }
    }
  }

  onChangeExpectedDeliveryTime(event: any) {
    this.deliveryForm.controls.expectedDeliveryTime.setValue(event.value);
    this.deliveryForm.controls.expectedDeliveryTime.markAsTouched();
  }

  onChangeCheckboxBIsR() {
    this.toggleValidateorInBuyerInfoForm();
    this.copyNamePhoneToReceiverForm();
  }

  private toggleValidateorInBuyerInfoForm() {
    if (this.buyerIsReceiver) {
      this.buyerInfoForm.get('name').setValidators([]);
      this.buyerInfoForm.get('name').updateValueAndValidity();
      this.buyerInfoForm.get('email').setValidators([]);
      this.buyerInfoForm.get('email').updateValueAndValidity();
      this.buyerInfoForm.get('phone').setValidators([]);
      this.buyerInfoForm.get('phone').updateValueAndValidity();
    } else {
      this.buyerInfoForm.get('name').setValidators([
        this.validatorService.checkRequired(),
      ]);
      this.buyerInfoForm.get('name').updateValueAndValidity();
      this.buyerInfoForm.get('email').setValidators([
        this.validatorService.checkRequired(),
        this.validatorService.checkEmail()
      ]);
      this.buyerInfoForm.get('email').updateValueAndValidity();
      this.buyerInfoForm.get('phone').setValidators([
        this.validatorService.checkRequired(),
        this.validatorService.checkNumberic(),
        this.validatorService.checkMinLength(10),
        this.validatorService.checkMaxLength(11)
      ]);
      this.buyerInfoForm.get('phone').updateValueAndValidity();
      this.markAsUntouchedForAll(this.buyerInfoForm);
    }
  }

  private _initBreadCrumb() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Đặt hàng',
        icon: 'pi pi-shopping-cart',
        routerLink: '/mua'
      },
      {
        label: 'Chọn ngày giờ, địa điểm giao hàng',
        icon: 'pi pi-map-marker'
      }
    ]);
  }

  private copyNamePhoneToReceiverForm() {
    this.deliveryForm.patchValue({
      name: this.buyerInfoForm.value.name,
      phone: this.buyerInfoForm.value.phone,
    });
  }
}
