import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { Product } from '../../detail-product/shared/models/Product.model';
import { AppTopBarComponent } from '../../shared/components/layout/app-topbar/app.topbar.component';
import { IOrderItem } from '../../shared/services/order.service';
import { environment } from '../../../environments/environment';
import SizeImage = GlobalConstant.SizeImage;
import { GlobalConstant } from '../../shared/constants/global.constant';
import { BasePage } from '../../shared/components/base/base-page';
import { Router } from '@angular/router';
import { SeoService } from '../../shared/services/seo.service';
import { TokenStorage } from '../../shared/services/token-storage.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
  providers: [
    DialogService,
    AppTopBarComponent
  ]
})
export class CartComponent extends BasePage implements OnInit {
  orderItems: IOrderItem[] = [];
  total: any;
  SizeImage = SizeImage;

  constructor(private router: Router,
              private tokenService: TokenStorage,
              private seoService: SeoService,
              private topBar: AppTopBarComponent) {
    super();
  }

  ngOnInit() {
    this.scrollTop();
    this.getCartItems();
    this._initBreadCrumb();
    this.seoService.setHeader('Giỏ hàng');
  }

  getCartItems() {
    const sub = this.orderService.getProductsInCart()
      .subscribe(
        (orderItems: IOrderItem[]) => {
          this.orderItems = orderItems;
          this.updateTotal();
          this.globalService.setLoadingSpinner(false);
        }
      );

    this.subscriptions.push(sub);
  }

  updateTotal() {
    this.total = this.orderItems.map(
      (item: any) => {
        if (item.product.saleOff.active) {
          return item.product.saleOff.price * item.quantity;
        }
        return item.product.originalPrice * item.quantity;
      }
    );

    const add = (calc, a) => {
      return calc + a;
    };

    this.total = this.total.reduce(add, 0);
  }

  onRemoveItem(id: string, index: number) {
    // TODO: switch case login and not login
    this.globalService.setLoadingSpinner(true);
    // this.orderItems = this.orderItems.filter((item: any) => item._id !== id);
    // this.updateTotal();
    this.orderService.removeCartItems(id, index);
    // setTimeout(() => {
    //   this.globalService.setLoadingSpinner(false);
    // }, 150);
  }

  onChangeTotal(product: Product, quantity: number) {
    this.globalService.setLoadingSpinner(true);
    this.orderService.addProductToCart(product, quantity);
  }

  onChangeQuantity($event: any, orderItem: IOrderItem) {
    this.orderService.updateProductQuantity(orderItem.product, orderItem.quantity);
  }

  onCheckout() {
    this.router.navigate(['/mua/giao-hang']);
  }

  private _initBreadCrumb() {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Đặt hàng',
        icon: 'pi pi-shopping-cart',
        routerLink: '/mua'
      }
    ]);
  }
}
