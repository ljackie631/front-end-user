import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasePage } from '../../shared/components/base/base-page';

@Component({
  selector: 'app-purchase-success',
  templateUrl: './purchase-success.component.html',
  styleUrls: ['./purchase-success.component.scss']
})
export class PurchaseSuccessComponent extends BasePage implements OnInit {
  id = null;

  constructor(private activatedRoute: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((value: any) => {
      this.id = value.id;
    });
    this.scrollTop();
    this.globalService.setBreadcrumbs([]);
  }

}
