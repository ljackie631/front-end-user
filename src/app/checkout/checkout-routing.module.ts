import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginCanActivate } from '../shared/guards/login';
import { CartComponent } from './cart/cart.component';
import { DeliveryComponent } from './delivery/delivery.component';
import { PaymentComponent } from './payment/payment.component';
import { PurchaseSuccessComponent } from './purchase-success/purchase-success.component';
import { CartCanActive } from '../shared/guards/cart';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: CartComponent
  },
  {
    path: 'giao-hang',
    component: DeliveryComponent,
    canActivate: []
  },
  {
    path: 'thanh-toan',
    component: PaymentComponent,
    canActivate: [
      CartCanActive
    ]
  },
  {
    path: 'dat-hang-thanh-cong',
    component: PurchaseSuccessComponent,
    canActivate: []
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckoutRoutingModule {}
