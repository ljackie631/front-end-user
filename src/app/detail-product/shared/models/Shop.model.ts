export interface ShopInfo {
  availableShipCountry?: boolean;
  createdAt?: Date;
  images?: [];
  name?: string;
  slug?: string;
  status?: number;
  updatedAt?: Date;
  user?: string;
  _id?: string;
}
  