export interface Product {
  _id: string;
  title: string;
  images?: string[];
  thumbnail: string;
  sku?: string;
  description?: string;
  topic?: number;
  specialOccasion?: number;
  design?: number;
  floret?: number;
  city?: string;
  district?: number;
  color?: number;
  priceRange?: number;
  slug?: string;
  seoUrl?: string;
  seoDescription?: string;
  seoImage?: string;
  originalPrice: number;
  shop?: string;
  saleOff: {
    price: number;
    startDate: Date;
    endDate: Date;
    active: boolean;
  };
  code?: string;
  updatedAt?: Date;
  createdAt?: Date;
  tags?: {
    slug: string;
    customSlug?: string;
    keyword: string
  }[];
  sold?: number;
  view?: number;
  freeShip?: boolean;
}
