import { Component, OnInit, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { BasePage } from '../shared/components/base/base-page';
import { GlobalConstant } from '../shared/constants/global.constant';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { IResSearchBoxResult, IResSearchContent, SearchService } from '../shared/services/search.service';
import { SeoService } from '../shared/services/seo.service';
import { Product } from './shared/models/Product.model';
import { ShopInfo } from './shared/models/Shop.model';
import { AppTopBarComponent } from '../shared/components/layout/app-topbar/app.topbar.component';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { GlobalService } from '../shared/services/global.service';
import SizeImage = GlobalConstant.SizeImage;
import { TokenStorage } from '../shared/services/token-storage.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.scss'],
  providers: [
    DialogService,
    DynamicDialogRef,
    AppTopBarComponent
  ]
})
export class DetailProductComponent extends BasePage implements OnInit {
  width: number;
  images: any[] = [];
  value: number;
  product: Product = null;
  quantity = 1;
  discount = 0;
  relatedProducts: Product[] = [];
  shopInfo: ShopInfo = null;
  searchQuery: any;

  @Input()
  set data(res: IResSearchContent) {
    if (res.status === HTTP_CODES.SUCCESS) {
      this.product = res.data.product;
      this.relatedProducts = res.data.relatedProducts;
      this.shopInfo = res.data.shopInfo;
      this._initBreadCrumb(this.product);

      if (this.product.saleOff.active === true) {
        this.discount = Math.round(100 - this.product.saleOff.price * 100 / this.product.originalPrice);
      }

      this.searchQuery = res.data.searchQuery;

      this.initGalleria();
      this.seoService.setHeader(this.product.title);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.width = event.target.innerWidth;
  }

  constructor(private seoService: SeoService,
              private searchService: SearchService,
              private router: Router,
              public globalService: GlobalService,
              private tokenStorage: TokenStorage) {
    super();
  }

  ngOnInit() {
    this.scrollTop();
  }

  initGalleria() {
    this.images = (this.product.images || []).map(url => {
      if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
        url = `${environment.staticImageSize}${SizeImage.S600}/${url}`;
      }

      return url;
    });

    if (this.images.length === 0) {
      this.images.push('https://placehold.it/300');
    }

    this.images = this.images.map(url => {
      return {
        source: url,
        title: ''
      };
    });
  }

  addToCart() {
    this.orderService.addProductToCart(this.product, this.quantity, this.shopInfo, (response) => {
      if (this.isLoggedUser()) {
        if (response.status !== HTTP_CODES.SUCCESS) {
          this.swalDialogService.openError(response.messages.join('.'), 1000);
        } else {
          this.swalDialogService.openSuccess('Thêm sản phẩm thành công', 1000);
        }
      } else {
        this.swalDialogService.openSuccess('Thêm sản phẩm thành công', 1000);
      }
    });
  }

  onSearch(searchBoxParams: any) {
    const sub = this.searchService.searchBox(searchBoxParams)
      .subscribe((res: IResSearchBoxResult) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.router.navigate([`danh-muc/${res.data.url}`]);
        }
      });

    this.subscriptions.push(sub);
  }

  buyNow() {
    this.orderService.addProductToCart(this.product, this.quantity, this.shopInfo, (response) => {
      if (this.isLoggedUser()) {
        if (response.status !== HTTP_CODES.SUCCESS) {
          this.swalDialogService.openError(response.messages.join('.'), 1000);
        } else {
          this.redirectToCart();
        }
      } else {
        this.redirectToCart();
      }
    });
  }

  redirectToCart() {
    this.router.navigate([`mua`]);
  }

  isLoggedUser(): boolean {
    return this.tokenStorage.getUserInfo() !== null;
  }
  private _initBreadCrumb(data) {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: data.title,
        routerLink: '/chi-tiet-san-pham/' + data.slug
      }
    ]);
  }
}
