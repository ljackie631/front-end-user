import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../shared/services/global.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { BasePage } from '../../shared/components/base/base-page';
import { ActivatedRoute, Router } from '@angular/router';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { AppTopBarComponent } from '../../shared/components/layout/app-topbar/app.topbar.component';
import { DialogService } from 'primeng/api';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  providers: [DialogService, AppTopBarComponent]
})
export class ResetPasswordComponent extends BasePage implements OnInit {

  resetPasswordForm: FormGroup;

  constructor(public globalService: GlobalService,
              private _fb: FormBuilder,
              private _route: ActivatedRoute,
              private _router: Router,
              private _topBar: AppTopBarComponent) {
    super();
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.resetPasswordForm = this._fb.group({
      password: ['', [this.validatorService.checkRequired()]],
      confirmedPassword: ['', [this.validatorService.checkRequired()]]
    }, {validators: this.validatorService.checkConfirmPassword()});
  }

  onSubmit() {
    const data = {
      token: this._route.snapshot.paramMap.get('token'),
      ...this.resetPasswordForm.value
    };
    this.globalService.setLoadingSpinner(true);
    const sub = this.authService.resetPassword(data).subscribe(
      (res: any) => {
        if (res.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openSuccess(res.messages.join('.'));
          this.globalService.setLoadingSpinner(false);
          this._router.navigate(['/']);
          this._topBar.show();
        } else {
          this.swalDialogService.openError(res.messages.join('.'));
        }
        this.globalService.setLoadingSpinner(false);
      }
    );
    this.subscriptions.push(sub);
  }

}
