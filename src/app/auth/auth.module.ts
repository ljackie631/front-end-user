import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { TabMenuModule } from 'primeng/tabmenu';

import {
  ButtonModule,
  InputTextModule,
  MessageModule,
  MessagesModule,
  PanelMenuModule,
  TabViewModule,
  RadioButtonModule, DialogModule
} from 'primeng/primeng';
import { ToastModule } from 'primeng/toast';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PopupComponent } from './popup/popup.component';
import { PopupForgotPasswordComponent } from './popup-forgot-password/popup-forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    PopupComponent,
    PopupForgotPasswordComponent,
    ResetPasswordComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastModule,
    MessagesModule,
    MessageModule,
    ButtonModule,
    InputTextModule,
    DynamicDialogModule,
    TabMenuModule,
    PanelMenuModule,
    TabViewModule,
    RadioButtonModule,
    DialogModule
  ],
  entryComponents: [
    LoginComponent,
    RegisterComponent,
    PopupComponent,
    PopupForgotPasswordComponent,
    ResetPasswordComponent
  ],
  exports: [PopupComponent]
})
export class AuthModule {
}
