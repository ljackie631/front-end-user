import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';
import { SeoService } from '../../shared/services/seo.service';
import { SalesChannelService } from '../sales-channel.service';

@Component({
  selector: 'app-shop-statistic',
  templateUrl: './shop-statistic.component.html',
  styleUrls: ['./shop-statistic.component.scss']
})
export class ShopStatisticComponent extends BasePage implements OnInit {
  statisticOrder: any = {};
  statisticMoney: any = {};

  constructor(private salesChannelService: SalesChannelService,
              private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Kênh người bán',
        routerLink: '/kenh-nguoi-ban',
        icon: 'pi pi-user',
      },
      {
        label: 'Thống kê',
        routerLink: '/kenh-nguoi-ban/thong-ke',
        icon: 'pi pi-th-large'
      }
    ]);

    this.getStatisticOrder();
    this.getStatisticMoney();
    this.seoService.setHeader('Kênh Người Bán - Thống Kê');
  }

  private getStatisticOrder() {
    const sub = this.salesChannelService.getStatisticOrder()
      .subscribe((response: any) => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.statisticOrder = response.data;
        }
      });
    this.subscriptions.push(sub);
  }

  private getStatisticMoney() {
    const sub = this.salesChannelService.getStatisticMoney()
      .subscribe((response: any) => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.statisticMoney = response.data;
        }
      });
    this.subscriptions.push(sub);
  }

}
