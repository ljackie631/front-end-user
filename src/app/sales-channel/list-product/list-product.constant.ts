export namespace ListProduct {
  export const PriceOrder = [
    {text: 'Không thứ tự', value: {sb: null, sd: null}},
    {text: 'Giá: Thấp đến cao', value: {sb: 'originalPrice', sd: 'ASC'}},
    {text: 'Giá: Cao xuống thấp', value: {sb: 'originalPrice', sd: 'DESC'}}
  ];
}
