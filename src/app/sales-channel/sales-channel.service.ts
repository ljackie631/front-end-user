import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IQueryParam, IResProductList } from './list-product/list-product.component';
import { Observable } from 'rxjs';
import { API } from '../shared/constants/api.constant';
import { IResUpdateProductsStatus } from '../profile/profile.service';
import { IEventOrderItemStatus } from './shop-order-list/shop-order-item/shop-order-item.component';
import {
  IQueryParam as IQueryParamForShoOrderpList,
  IResShopOrderList,
  IResUpdateOrderStatus
} from './shop-order-list/shop-order-list.component';

@Injectable()
export class SalesChannelService {
  constructor(private httpClient: HttpClient) {
  }

  getListProduct(params: IQueryParam): Observable<IResProductList> {
    const _params: any = {...params};
    Object.keys(_params).forEach(key => {
      if (!_params[key]) {
        delete _params[key];
      } else {
        _params[key] = params[key].toString();
      }
    });
    return this.httpClient.get<IResProductList>(API.Shop.ShopProduct, {params: _params});
  }

  updateProductsStatus(productIds: string[], status: number): Observable<IResUpdateProductsStatus> {
    return this.httpClient.post<IResUpdateProductsStatus>(API.Shop.UpdateStatusOfProducts, {productIds, status});
  }

  getShopOrderList(params: IQueryParamForShoOrderpList): Observable<IResShopOrderList> {
    const _params: any = {...params};
    Object.keys(_params).forEach(key => {
      if (!_params[key]) {
        delete _params[key];
      } else {
        _params[key] = params[key].toString();
      }
    });
    return this.httpClient.get<IResShopOrderList>(API.Shop.GetOrderList, {params: _params});
  }

  updateOrderStatus(params: IEventOrderItemStatus): Observable<IResUpdateOrderStatus> {
    return this.httpClient.put<IResUpdateOrderStatus>(API.Shop.UpdateOrderStatus, params);
  }

  getStatisticOrder(): Observable<any> {
    return this.httpClient.get<any>(API.Shop.StatisticOrder);
  }

  getStatisticMoney(): Observable<any> {
    return this.httpClient.get<any>(API.Shop.StatisticMoney);
  }
}
