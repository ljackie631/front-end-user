import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SalesChannelComponent } from './sales-channel.component';
import { ShopCanActivate } from '../shared/guards/shop';
import { ListProductComponent } from './list-product/list-product.component';
import { NewProductComponent } from './new-product/new-product.component';
import { ShopOrderListComponent } from './shop-order-list/shop-order-list.component';
import { ShopStatisticComponent } from './shop-statistic/shop-statistic.component';
import { UpdateShopInfoComponent } from './update-shop-info/update-shop-info.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      component: SalesChannelComponent,
      children: [
        {
          path: 'danh-sach-san-pham',
          component: ListProductComponent,
          canActivate: [ShopCanActivate]
        },
        {
          path: 'dang-san-pham',
          component: NewProductComponent,
          canActivate: [ShopCanActivate]
        },
        {
          path: 'sua-san-pham/:id',
          component: NewProductComponent,
          canActivate: [ShopCanActivate]
        },
        {
          path: 'don-dat-hang',
          component: ShopOrderListComponent,
          canActivate: [ShopCanActivate]
        },
        {
          path: 'thong-ke',
          component: ShopStatisticComponent,
          canActivate: [ShopCanActivate]
        }
        ,
        {
          path: 'chinh-sua-thong-tin-shop',
          component: UpdateShopInfoComponent,
          canActivate: [ShopCanActivate]
        }
      ]
    }
  ])],
  exports: []
})

export class SalesChannelRoutingModule {
}
