import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GlobalConstant } from '../../shared/constants/global.constant';
import { IProduct } from '../list-product/list-product.component';
import { environment } from "../../../environments/environment";
import SizeImage = GlobalConstant.SizeImage;
export interface IProduct {
  _id: string;
  title: string;
  images: string[];
  thumbnail?: string; // just use in FE
  sku: string;
  description: string;
  topic: number;
  specialOccasion?: number;
  design?: number;
  floret?: number;
  city?: string;
  district?: number;
  color?: number;
  priceRange: number;
  slug: string;
  seoUrl?: string;
  seoDescription?: string;
  seoImage?: string;
  originalPrice: number;
  code: string;
  updatedAt: Date;
  createdAt: Date;
  user: string;
  status: number;
  view: number;
  sold?: number;
  addToCart: number;
  saleOff: {
    price: number;
    startDate: Date;
    endDate: Date;
    active: boolean;
  };
}

@Component({
  selector: 'app-shop-product-item-table',
  templateUrl: './product-item-table.component.html',
  styleUrls: ['./product-item-table.component.scss']
})

export class ShopProductItemTableComponent {
  private _listProduct: IProduct[] = null;
  status = GlobalConstant.Status;


  SizeImage = SizeImage;


  _listProductCheckObj: any = {};

  _selectAllProduct = false;

  _numberOfSelected = 0;

  @Input()
  set numberOfSelected(value) {
    this._numberOfSelected = value;
  }

  get numberOfSelected() {
    return this._numberOfSelected;
  }

  @Input()
  set listProduct(value) {
    this._listProduct = value;
    this._listProduct = this._listProduct.map(item => {
      return this.mapImage(item);
    });
  }

  get listProduct(): IProduct[] {
    return this._listProduct;
  }

  @Input()
  set selectAllProduct(value) {
    this._selectAllProduct = value;
  }

  get selectAllProduct() {
    return this._selectAllProduct;
  }

  @Input()
  set listProductCheckObj(value) {
    this._listProductCheckObj = value;
  }

  get listProductCheckObj() {
    return this._listProductCheckObj;
  }

  @Input() value: boolean;


  @Input() disabledCheckbox = false;

  @Output() valueChange = new EventEmitter();
  @Output() changeSelection = new EventEmitter();

  onChangeCheckBox() {
    this.countSelection();
    this.changeSelection.emit();
  }

  countSelection() {
    this._numberOfSelected = Object.values(this.listProductCheckObj)
      .filter(v => v === true)
      .length;

    if (this._numberOfSelected === this.listProduct.length && this.listProduct.length !== 0) {
      this.selectAllProduct = true;
    } else {
      this.selectAllProduct = false;
    }
  }

  mapImage = (product: IProduct): IProduct  => {
    if (product.images) {
      product.thumbnail = product.images[0] || 'http://placehold.it/300';
    }
    return product;
  }


}
