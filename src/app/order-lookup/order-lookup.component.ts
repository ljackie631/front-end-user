import { Component, OnInit } from '@angular/core';
import { BasePage } from '../shared/components/base/base-page';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { GlobalConstant } from '../shared/constants/global.constant';
import Status = GlobalConstant.Status;

@Component({
  selector: 'app-order-lookup',
  templateUrl: './order-lookup.component.html',
  styleUrls: ['./order-lookup.component.scss'],
})

export class OrderLookupComponent extends BasePage implements OnInit {
  orderId: string;
  orderResult: any;
  orderStatus = Status;
  notifyFlag = true;

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Tra cứu đơn hàng',
        icon: 'pi pi-info-circle',
        routerLink: '/tra-cuu-don-hang'
      }
    ]);
  }

  lookUpOrder() {
    this.orderResult = null;
    const sub = this.orderService.lookUpOrder(this.orderId)
      .subscribe(response => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.orderResult = response.data;
          this.notifyFlag = true;
        } else {
          this.notifyFlag = false;
        }
      });
    this.subscriptions.push(sub);
  }
}
