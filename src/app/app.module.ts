import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  BreadcrumbModule, MessageService,
  ProgressSpinnerModule
} from 'primeng/primeng';
import { GrowlModule } from 'primeng/growl';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DetailPostResolverModule } from './detail-post-resolver/detail-post-resolver.module';
import { HomeModule } from './home/home.module';
import { AppLayoutModule } from './shared/components/layout/app-layout.module';
import { GuardModule } from './shared/guards/guard.module';
import { AuthService } from './shared/services/auth.service';
import { GlobalService } from './shared/services/global.service';
import { ServiceLocator } from './shared/services/locator.service';
import { OrderService } from './shared/services/order.service';
import { SeoService } from './shared/services/seo.service';
import { TokenInterceptor } from './shared/services/token-interceptor.service';
import { ValidatorService } from './shared/services/validator.service';
import { AuthModule } from './auth/auth.module';
import { SearchResultsModule } from './search-results/search-results.module';
import { DetailProductModule } from './detail-product/detail-product.module';
import { TokenStorage } from './shared/services/token-storage.service';
import { CookieService } from 'ngx-cookie-service';
import { ProfileService } from './profile/profile.service';
import { ConfirmAccountModule } from './confirm-account/confirm-account.module';
import { InitializationApplicationService, validateAccessToken } from './shared/services/init-app.service';
import { SocketMessageService } from './shared/services/socket-message.service';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { SwalDialogService } from './shared/services/swal-dialog.service';
import { SalesChannelService } from './sales-channel/sales-channel.service';
import { environment } from '../environments/environment';
import { OrderLookupModule } from './order-lookup/order-lookup.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    SweetAlert2Module.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ProgressSpinnerModule,
    AppLayoutModule,
    HomeModule,
    AuthModule,
    BreadcrumbModule,
    SearchResultsModule,
    DetailProductModule,
    DetailPostResolverModule,
    ConfirmAccountModule,
    GrowlModule,
    GuardModule,
    OrderLookupModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {provide: 'API_GOOGLE_MAP_TOKEN', useValue: environment.googleMapToken},
    ServiceLocator,
    ValidatorService,
    SocketMessageService,
    AuthService,
    TokenStorage,
    CookieService,
    GlobalService,
    MessageService,
    SwalDialogService,
    ProfileService,
    SalesChannelService,
    SeoService,
    OrderService,
    InitializationApplicationService,
    {
      provide: APP_INITIALIZER,
      useFactory: validateAccessToken,
      deps: [
        InitializationApplicationService
      ],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private injector: Injector) {
    ServiceLocator.injector = injector;
  }
}
