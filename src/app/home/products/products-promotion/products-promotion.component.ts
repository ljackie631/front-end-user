import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../shared/models/product.model';
import { HomeService } from '../../../shared/services/home.service';

@Component({
  selector: 'app-products-promotion',
  templateUrl: './products-promotion.component.html',
  styleUrls: ['./products-promotion.component.scss']
})
export class ProductsPromotionComponent implements OnInit {
  @Input() saleProducts: Product[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }
}
