import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../shared/models/product.model';
import { HomeService } from '../../../shared/services/home.service';

@Component({
  selector: 'app-products-featured',
  templateUrl: './products-featured.component.html',
  styleUrls: ['./products-featured.component.scss']
})
export class ProductsFeaturedComponent implements OnInit {
  @Input() featuredProducts: Product[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }
}
