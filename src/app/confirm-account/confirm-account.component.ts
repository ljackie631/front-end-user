import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BasePage } from '../shared/components/base/base-page';
import { HTTP_CODES } from '../shared/constants/http-codes.constant';
import { SeoService } from '../shared/services/seo.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { DialogService } from 'primeng/api';
import { PopupComponent } from '../auth/popup/popup.component';

@Component({
  selector: 'app-confirm-account',
  templateUrl: './confirm-account.component.html',
  styleUrls: ['./confirm-account.component.scss'],
  providers: [DialogService]
})
export class ConfirmAccountComponent extends BasePage implements OnInit {
  resendForm: FormGroup;
  isCorrectToken: any = null;

  constructor(private activatedRoute: ActivatedRoute,
              private seoService: SeoService,
              private _router: Router,
              private dialogService: DialogService,
              private _fb: FormBuilder) {
    super();

    if (this.globalService.isPlatformBrowser()) {
      this.activatedRoute.params.subscribe((params: any) => {
        this.globalService.setLoadingSpinner(true);
        const sub = this.authService.confirmAccount(params.token)
          .subscribe((response) => {
            if (response.status === HTTP_CODES.SUCCESS) {
              this.isCorrectToken = true;
            } else {
              this.isCorrectToken = false;
            }

            this.globalService.setLoadingSpinner(false);
          });
        this.subscriptions.push(sub);
      });
    }
  }

  ngOnInit(): void {
    this.seoService.setHeader('Trang cá nhân - Xác nhận tài khoản');
    this.initForm();
  }

  initForm() {
    this.resendForm = this._fb.group({
      email: ['', [this.validatorService.checkRequired(), this.validatorService.checkEmail()]]
    });
  }

  showLogin() {
    this._router.navigate(['/']);
    this.dialogService.open(PopupComponent, {
      showHeader: false,
      width: '80%',
      closeOnEscape: true,
      style: {
        'max-width': '600px'
      },
      contentStyle: {
        'max-height': '50%',
        overflow: 'auto'
      }
    });
  }

  onSubmit() {
    const sub = this.authService.resendConfirmAccount(this.resendForm.value).subscribe(
      (response: any) => {
        if (response.status === HTTP_CODES.SUCCESS) {
          this.swalDialogService.openInfo(response.messages[0].join('.'));
        } else {
          this.swalDialogService.openWarning(response.messages[0].join('.'));
        }
      }
    );
    this.subscriptions.push(sub);
  }
}
