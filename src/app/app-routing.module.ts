import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfirmAccountComponent } from './confirm-account/confirm-account.component';
import { HomeComponent } from './home/home.component';
import { DetailPostResolverComponent } from './detail-post-resolver/detail-post-resolver.component';
import { LoginCanActivate } from './shared/guards/login';
import { ResetPasswordComponent } from './auth/reset-password/reset-password.component';
import { OrderLookupComponent } from './order-lookup/order-lookup.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: 'xac-nhan-tai-khoan/:token',
    component: ConfirmAccountComponent
  },
  {
    path: 'dat-lai-mat-khau/:token',
    component: ResetPasswordComponent
  },
  {
    path: 'mua',
    loadChildren: './checkout/checkout.module#CheckoutModule'
  },
  {
    path: 'demo',
    loadChildren: './demo/demo.module#DemoModule'
  },
  {
    path: 'tin-tuc',
    loadChildren: './news/news.module#NewsModule'
  },
  {
    path: 'khuyen-mai',
    loadChildren: './sale-products/sale-products.module#SaleProductsModule'
  },
  {
    path: 'noi-bat',
    loadChildren: './featured-products/featured-products.module#FeaturedProductsModule'
  },
  {
    path: 'dang-ki-ban-hang',
    loadChildren: './register-shop/register-shop.module#RegisterShopModule'
  },
  {
    path: 'trang-ca-nhan',
    loadChildren: './profile/profile.module#ProfileModule',
    canActivate: [LoginCanActivate]
  },
  {
    path: 'kenh-nguoi-ban',
    loadChildren: './sales-channel/sales-channel.module#SalesChannelModule',
    canActivate: [LoginCanActivate]
  },
  {
    path: 'tra-cuu-don-hang',
    component: OrderLookupComponent
  },
  {
    path: '**',
    component: DetailPostResolverComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
