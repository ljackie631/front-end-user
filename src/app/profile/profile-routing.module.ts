import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ListAddressComponent } from './address-list/address-list.component';
import { NotificationComponent } from './notification/notification.component';
import { UpdateInfoUserComponent } from './update-info-user/update-info-user.component';
import { RegisterShopComponent } from './register-shop/register-shop.component';
import { RechargeComponent } from './recharge/recharge.component';
import { UserOrderListComponent } from './user-order-list/user-order-list.component';
import { UserOrderDetailComponent } from './user-order-detail/user-order-detail.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      component: ProfileComponent,
      children: [
        {
          path: 'so-dia-chi',
          component: ListAddressComponent
        },
        {
          path: 'thong-bao',
          component: NotificationComponent
        },
        {
          path: 'thong-tin-ca-nhan',
          component: UpdateInfoUserComponent
        },
        {
          path: 'dang-ky-ban-hang',
          component: RegisterShopComponent
        },
        {
          path: 'nap-tien-vao-tai-khoan',
          component: RechargeComponent
        },
        {
          path: 'don-hang',
          component: UserOrderListComponent
        },
        {
          path: 'don-hang/chi-tiet/:id',
          component: UserOrderDetailComponent
        }
      ]
    }
  ])],
  exports: [RouterModule]
})
export class ProfileRoutingModule {

}
