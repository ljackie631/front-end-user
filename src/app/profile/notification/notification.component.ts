import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { SeoService } from '../../shared/services/seo.service';
import { ProfileService } from '../profile.service';

interface INotification {
  content: string;
  createdAt: Date;
  fromUser?: null;
  params?: object;
  status: number;
  title: string;
  toUser?: string;
  type: number;
  updatedAt: Date;
  _id: string;
}

interface IMetaNotification {
  item: number;
  limit: number;
  page: number;
  totalItems: number;
}

@Component({
  selector: 'app-profile-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})

export class NotificationComponent extends BasePage implements OnInit {
  listNofitication: INotification[] = [];
  metaNotification: IMetaNotification;
  constructor(private profileService: ProfileService,
              private seoService: SeoService) {
    super();
  }
  ngOnInit(): void {
    this.getListNotifications();
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Trang cá nhân',
        routerLink: '/trang-ca-nhan',
        icon: 'pi pi-user',
      },
      {
        label: 'Thông báo',
        routerLink: '/trang-ca-nhan/thong-bao',
        icon: 'pi pi-th-large'
      }
    ]);
  }

  getListNotifications(page = 1) {
    const sub = this.profileService.getNotifications(page).subscribe(
      (res: any) => {
        this.listNofitication = res.data.notifies;
        this.metaNotification = res.data.meta;
      }
    );
    this.subscriptions.push(sub);
    this.seoService.setHeader('Trang cá nhân - Thông báo của tôi');
  }

  onPaginate(event: any) {
    this.getListNotifications(event.page + 1);
  }

}
