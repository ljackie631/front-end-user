import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API } from '../shared/constants/api.constant';
import { IBody, IResAddressList } from './address-list/address-list.component';
import { IResShop } from './register-shop/register-shop.component';
import { IResOrderDetail } from './user-order-detail/user-order-detail.component';
import { IInfoUserBody, IResUserInfo } from './update-info-user/update-info-user.component';

export interface IResUpdateProductsStatus {
  status: number;
  messages: string[];
}

@Injectable()
export class ProfileService {
  constructor(private httpClient: HttpClient) {
  }

  getAddressList(): Observable<IResAddressList> {
    return this.httpClient.get<IResAddressList>(API.Address.GetListAddress);
  }

  addNewAddress(body: IBody): Observable<IResAddressList> {
   return this.httpClient.post<IResAddressList>(API.Address.AddNewAddress, body);
  }
  changeAddress(body: IBody, addressId: string): Observable<IResAddressList> {
    return this.httpClient.put<IResAddressList>(API.Address.ChangeAddress.replace(':id', addressId), body);
  }

  removeAddress(addressId: string): Observable<IResAddressList> {
    return this.httpClient.delete<IResAddressList>(API.Address.RemoveAddress.replace(':id', addressId));
  }

  checkSlugExist(slug: string): Observable<IResShop> {
    return this.httpClient.get<IResShop>(API.Shop.CheckSlugExist, {params: {slug}});
  }

  registerShop(body: IBody): Observable<IResShop> {
    return this.httpClient.post<IResShop>(API.Shop.RegisterShop, body);
  }

  updateShop(body: IBody): Observable<IResShop> {
    return this.httpClient.put<IResShop>(API.Shop.UpdateShop, body);
  }

  getDetailShop(): Observable<IResShop> {
    return this.httpClient.get<IResShop>(API.Shop.DetailShop);
  }

  getOrderDetail(orderId: string): Observable<IResOrderDetail> {
    return this.httpClient.get<IResOrderDetail>(API.Order.GetOrderDetail.replace(':orderId', orderId));
  }

  updateUserInfo(body: IInfoUserBody): Observable<IResUserInfo> {
    return this.httpClient.put<IResUserInfo>(API.Profile.UpdateUserInfo, body);
  }

  getNotifications(page: number, limit: number = 20): Observable<any> {
    let query = new HttpParams();
    query = query.append('page', String(page));
    query = query.append('limit', String(limit));
    return this.httpClient.get(API.Notify.GetNotify, {params: query});
  }
}
