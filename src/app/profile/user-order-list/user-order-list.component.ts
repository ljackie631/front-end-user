import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { SeoService } from '../../shared/services/seo.service';

@Component({
  selector: 'app-user-order-list-component',
  templateUrl: './user-order-list.component.html',
  styleUrls: [
    './user-order-list.component.scss'
  ]
})
export class UserOrderListComponent extends BasePage implements OnInit {
  orders: any[] = [];
  constructor(private seoService: SeoService) {
    super();
  }

  ngOnInit(): void {
    this._loadOrders();
    this.seoService.setHeader('Trang cá nhân - Quản lý đơn hàng');
  }

  private _loadOrders() {
    this.orderService.getUserOrders()
      .subscribe((response: any) => {
        console.log(response);
        this.orders = response.data;
      });
  }
}
