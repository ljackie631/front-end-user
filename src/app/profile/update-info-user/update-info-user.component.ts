import { Component, OnInit } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { SeoService } from '../../shared/services/seo.service';
import { ILoggedInUser } from '../../shared/services/auth.service';
import { ProfileService } from '../profile.service';
import { HTTP_CODES } from '../../shared/constants/http-codes.constant';

export interface IInfoUserBody {
  name?: string;
  phone?: number;
  birthday?: Date;
  gender?: number;
  avatar?: {
    link?: string;
  };
  password?: string;
  newPassword?: string;
  confirmedPassword?: string;
}

export interface UserInformation {
  name: string;
  phone: number;
  birthday: Date;
  gender: number;
  avatar?: string;
  password?: string;
  newPassword?: string;
  confirmedPassword?: string;
}

export interface IResUserInfo {
  status: number;
  messages: string[];
  data: {
    userInfoResponse: ILoggedInUser;
  };
}

@Component({
  selector: 'app-profile-update-info-user',
  templateUrl: './update-info-user.component.html',
  styleUrls: ['./update-info-user.component.scss']
})

export class UpdateInfoUserComponent extends BasePage implements OnInit {
  userInfo: UserInformation = {
    name: '',
    phone: null,
    birthday: null,
    gender: 1,
    avatar: null,
    password: '',
    newPassword: '',
    confirmedPassword: ''
  };
  passwordFlag = true;
  confirmedPasswordFlag = true;
  passwordLengthFlag = true;
  correctPassword = true;
  imageExist = true;

  constructor(private seoService: SeoService,
              private profileService: ProfileService) {
    super();
  }

  ngOnInit(): void {
    this.globalService.setBreadcrumbs([
      {
        label: 'Trang chủ',
        icon: 'pi pi-home',
        routerLink: '/'
      },
      {
        label: 'Trang cá nhân',
        routerLink: '/trang-ca-nhan',
        icon: 'pi pi-user',
      },
      {
        label: 'Thông tin tài khoản',
        routerLink: '/trang-ca-nhan/thong-tin-ca-nhan',
        icon: 'pi pi-th-large'
      }
    ]);
    this.initializeUserInfo();
    this.seoService.setHeader('Trang cá nhân');
  }

  isCorrectPassword(newPassword: string, confirmedPassword: string, password: string): boolean {
    if (newPassword !== '' || confirmedPassword !== '') {
      this.passwordFlag = true;
      this.confirmedPasswordFlag = true;
      this.passwordLengthFlag = true;
      if (newPassword.length < 6 || confirmedPassword.length < 6) {
        this.passwordLengthFlag = false;
        return false;
      }
      if (newPassword !== confirmedPassword && password === '') {
        this.passwordFlag = false;
        this.confirmedPasswordFlag = false;
        return false;
      }
      if (newPassword !== confirmedPassword && password !== '') {
        this.confirmedPasswordFlag = false;
        return false;
      }
      if (newPassword === confirmedPassword && password === '') {
        this.passwordFlag = false;
        return false;
      }
    }
    this.passwordLengthFlag = true;
    this.passwordFlag = true;
    this.confirmedPasswordFlag = true;
    return true;
  }

  initializeUserInfo() {
    this.authService.getUserInfo().subscribe(data => {
      if (data !== null) {
        this.turnLoggedInUserIntoUserInfo(data);
      }
    });
  }

  updateUserInfo() {
    this.correctPassword = this.isCorrectPassword(this.userInfo.newPassword, this.userInfo.confirmedPassword, this.userInfo.password);
    this.imageExist = this.isImageExist();
    if (this.correctPassword && this.imageExist) {
      const body = this.turnUserInfoIntoBody(this.userInfo);
      const sub = this.profileService.updateUserInfo(body)
        .subscribe((response: IResUserInfo) => {
          this.initMessage(response, 'Thay đổi thông tin thành công');
        });
      this.subscriptions.push(sub);
    }
  }

  uploadImage(event: any) {
    if (event.length !== 0) {
      this.userInfo.avatar = event[0].link;
    } else {
      this.userInfo.avatar = null;
    }
  }

  turnUserInfoIntoBody(userInfo: UserInformation): IInfoUserBody {
    const body: IInfoUserBody = userInfo.avatar !== null ? {avatar: {link: userInfo.avatar}} : {};
    Object.keys(userInfo)
      .filter((key: string) => key !== 'avatar')
      .forEach(key => {
        if (userInfo[key] !== null && userInfo[key] !== '') {
          body[key] = userInfo[key];
        }
    });
    return body;
  }

  turnLoggedInUserIntoUserInfo(loggedUser: ILoggedInUser) {
    Object.keys(this.userInfo).forEach(key => {
      if (loggedUser[key] !== null && loggedUser[key] !== undefined && loggedUser[key] !== '') {
        this.userInfo[key] = loggedUser[key];
      }
    });
    this.userInfo.birthday = this.userInfo.birthday !== null ? new Date(this.userInfo.birthday) : null;
  }

  initMessage(response: IResUserInfo, successfulDetail: string) {
    if (response.status === HTTP_CODES.SUCCESS) {
      this.messageService.add({severity: 'success', summary: 'Thông Báo', detail: successfulDetail, life: 10000});
      this.updateCookieAfterUpdate(response.data.userInfoResponse);
    } else {
      let errorMessage = '';
      response.messages.forEach(event => {
        errorMessage += event + '.';
      });
      this.messageService.add({severity: 'error', summary: 'Thông Báo', detail: errorMessage, life: 10000});
    }
  }

  updateCookieAfterUpdate(updatedData: ILoggedInUser) {
    this.authService.saveUserInto(updatedData);
    this.initializeUserInfo();
  }

  isImageExist(): boolean {
    return this.userInfo.avatar !== null;
  }
}
