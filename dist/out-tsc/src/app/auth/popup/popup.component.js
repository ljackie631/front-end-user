import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PopupComponent = /** @class */ (function () {
    function PopupComponent() {
    }
    PopupComponent.prototype.ngOnInit = function () {
        this.googleInit();
        this.facebookInit();
    };
    PopupComponent.prototype.facebookInit = function () {
        window.fbAsyncInit = function () {
            FB.init({
                appId: '2293629277515924',
                cookie: true,
                xfbml: true,
                version: 'v3.2'
            });
            FB.AppEvents.logPageView();
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        })(document, 'script', 'facebook-jssdk');
    };
    PopupComponent.prototype.submitLogin = function () {
        FB.login(function (response) {
            if (response.authResponse) {
                console.log(response);
            }
            else {
                console.log('User login failed');
            }
        });
    };
    PopupComponent.prototype.googleInit = function () {
        var _this = this;
        gapi.load("auth2", function () {
            _this.auth2 = gapi.auth2.init({
                client_id: "183898267483-amp43ntf1ie24ded0t6h0ag5ta9sts97.apps.googleusercontent.com",
                cookiepolicy: "single_host_origin",
                scope: "profile email"
            });
            _this.attachSignin(document.getElementById("googleBtn"));
        });
    };
    PopupComponent.prototype.attachSignin = function (element) {
        this.auth2.attachClickHandler(element, {}, function (googleUser) {
            var profile = googleUser.getBasicProfile();
            console.log("Token: " + googleUser.getAuthResponse().access_token);
            console.log('ID: ' + profile.getId());
            console.log('Full Name: ' + profile.getName());
            console.log('Given Name: ' + profile.getGivenName());
            console.log('Family Name: ' + profile.getFamilyName());
            console.log('Image URL: ' + profile.getImageUrl());
            console.log('Email: ' + profile.getEmail());
        }, function (error) {
        });
    };
    PopupComponent.prototype.onSignIn = function (googleUser) {
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Do not send to your backend! Use an ID token instead.
        console.log("Name: " + profile.getName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail()); // This is null if the 'email' scope is not present.
    };
    PopupComponent.prototype.signOut = function () {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log("User signed out.");
        });
    };
    PopupComponent = tslib_1.__decorate([
        Component({
            selector: 'app-popup',
            templateUrl: './popup.component.html',
            styleUrls: ['./popup.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PopupComponent);
    return PopupComponent;
}());
export { PopupComponent };
//# sourceMappingURL=popup.component.js.map