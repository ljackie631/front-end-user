import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var PRICE_OPTIONS = [
    { label: 'Từ thấp đến cao', value: 0 },
    { label: 'Từ cao đến thấp', value: 1 }
];
var SearchResultsComponent = /** @class */ (function () {
    function SearchResultsComponent() {
        this.priceOptions = PRICE_OPTIONS;
    }
    SearchResultsComponent.prototype.ngOnInit = function () {
    };
    SearchResultsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-search-results',
            templateUrl: './search-results.component.html',
            styleUrls: ['./search-results.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], SearchResultsComponent);
    return SearchResultsComponent;
}());
export { SearchResultsComponent };
//# sourceMappingURL=search-results.component.js.map