import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Products } from '../../shared/constants/products.constant';
var NewProductsComponent = /** @class */ (function () {
    function NewProductsComponent() {
    }
    NewProductsComponent.prototype.ngOnInit = function () {
        this.products = Products;
    };
    NewProductsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-new-products',
            templateUrl: './new-products.component.html',
            styleUrls: ['./new-products.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], NewProductsComponent);
    return NewProductsComponent;
}());
export { NewProductsComponent };
//# sourceMappingURL=new-products.component.js.map