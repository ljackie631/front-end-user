import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Products } from '../../shared/constants/products.constant';
var PopularProductsComponent = /** @class */ (function () {
    function PopularProductsComponent() {
    }
    PopularProductsComponent.prototype.ngOnInit = function () {
        this.products = Products;
    };
    PopularProductsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-popular-products',
            templateUrl: './popular-products.component.html',
            styleUrls: ['./popular-products.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], PopularProductsComponent);
    return PopularProductsComponent;
}());
export { PopularProductsComponent };
//# sourceMappingURL=popular-products.component.js.map