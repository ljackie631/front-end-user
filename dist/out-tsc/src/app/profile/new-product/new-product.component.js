import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { SearchSelector } from '../../shared/constants/search-selector.constant';
var NewProductComponent = /** @class */ (function (_super) {
    tslib_1.__extends(NewProductComponent, _super);
    function NewProductComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.searchSelector = SearchSelector;
        _this.showSelectSpecialOccasion = false;
        return _this;
    }
    NewProductComponent.prototype.onTopicChanged = function (event) {
        this.showSelectSpecialOccasion = event.value.value === 9;
    };
    NewProductComponent.prototype.ngOnInit = function () {
        this.globalService.setBreadcrumbs([
            {
                label: 'Trang chủ',
                icon: 'pi pi-home',
                routerLink: '/'
            },
            {
                label: 'Trang cá nhân',
                routerLink: '/trang-ca-nhan',
                icon: 'pi pi-user',
            },
            {
                label: 'Đăng sản phẩm',
                routerLink: '/trang-ca-nhan/dang-san-pham',
                icon: 'pi pi-th-large'
            }
        ]);
    };
    NewProductComponent = tslib_1.__decorate([
        Component({
            selector: 'app-new-product',
            templateUrl: './new-product.component.html',
            styleUrls: ['./new-product.component.scss']
        })
    ], NewProductComponent);
    return NewProductComponent;
}(BasePage));
export { NewProductComponent };
//# sourceMappingURL=new-product.component.js.map