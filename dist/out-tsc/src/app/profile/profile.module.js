import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule, CalendarModule, CardModule, ChipsModule, DialogModule, DropdownModule, InputMaskModule, InputTextareaModule, InputTextModule, PaginatorModule, PasswordModule, RadioButtonModule, TabViewModule, ToolbarModule } from 'primeng/primeng';
import { ImageUploaderModule } from '../shared/components/image-uploader/imageUploader.module';
import { ListProductComponent } from './list-product/list-product.component';
import { NewProductComponent } from './new-product/new-product.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';
import { ProfileService } from './profile.service';
import { ListAddressComponent } from './address-list/address-list.component';
import { NotificationComponent } from './notification/notification.component';
import { UpdateInfoUserComponent } from './update-info-user/update-info-user.component';
var ProfileModule = /** @class */ (function () {
    function ProfileModule() {
    }
    ProfileModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                ProfileComponent,
                NewProductComponent,
                ListProductComponent,
                ListAddressComponent,
                NotificationComponent,
                UpdateInfoUserComponent
            ],
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                ProfileRoutingModule,
                InputMaskModule,
                InputTextareaModule,
                ChipsModule,
                ButtonModule,
                ImageUploaderModule,
                CardModule,
                InputTextModule,
                DropdownModule,
                ButtonModule,
                ToolbarModule,
                TabViewModule,
                PaginatorModule,
                ToolbarModule,
                DialogModule,
                RadioButtonModule,
                CalendarModule,
                PasswordModule
            ],
            providers: [
                ProfileService
            ],
            exports: []
        })
    ], ProfileModule);
    return ProfileModule;
}());
export { ProfileModule };
//# sourceMappingURL=profile.module.js.map