import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var ProfileComponent = /** @class */ (function () {
    function ProfileComponent() {
    }
    ProfileComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.component.html',
            styleUrls: ['./profile.component.scss']
        })
    ], ProfileComponent);
    return ProfileComponent;
}());
export { ProfileComponent };
//# sourceMappingURL=profile.component.js.map