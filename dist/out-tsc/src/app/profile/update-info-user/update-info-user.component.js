import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
var UpdateInfoUserComponent = /** @class */ (function (_super) {
    tslib_1.__extends(UpdateInfoUserComponent, _super);
    function UpdateInfoUserComponent() {
        var _this = _super.call(this) || this;
        _this.correctPassword = true;
        return _this;
    }
    UpdateInfoUserComponent.prototype.ngOnInit = function () {
        this.globalService.setBreadcrumbs([
            {
                label: 'Trang chủ',
                icon: 'pi pi-home',
                routerLink: '/'
            },
            {
                label: 'Trang cá nhân',
                routerLink: '/trang-ca-nhan',
                icon: 'pi pi-user',
            },
            {
                label: 'Thông tin tài khoản',
                routerLink: '/trang-ca-nhan/thong-tin-ca-nhan',
                icon: 'pi pi-th-large'
            }
        ]);
        this.initializeUserInfo();
    };
    UpdateInfoUserComponent.prototype.isCorrectPassword = function (newPassword, repeatPassword) {
        if (newPassword !== repeatPassword) {
            return false;
        }
        return true;
    };
    UpdateInfoUserComponent.prototype.initializeUserInfo = function () {
        var _this = this;
        this.authService.getUserInfo().subscribe(function (data) {
            if (data === undefined || data === null) {
                // Dummy data for user info
                _this.userInfo = {
                    name: 'Lanh Viet Thuan',
                    email: '',
                    phone: 19008198,
                    birthday: new Date('11/09/2000'),
                    gender: 1
                };
            }
            // TODO : if data not null, assign value to user info.
        });
    };
    UpdateInfoUserComponent.prototype.updateUserInfo = function (userInfo, newPassword, repeatPassword) {
        this.correctPassword = this.isCorrectPassword(newPassword, repeatPassword);
    };
    UpdateInfoUserComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile-update-info-user',
            templateUrl: './update-info-user.component.html',
            styleUrls: ['./update-info-user.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], UpdateInfoUserComponent);
    return UpdateInfoUserComponent;
}(BasePage));
export { UpdateInfoUserComponent };
//# sourceMappingURL=update-info-user.component.js.map