import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { BasePage } from '../../shared/components/base/base-page';
import { MonthSelector } from './notification.constant';
import { ProfileService } from '../profile.service';
var NotificationComponent = /** @class */ (function (_super) {
    tslib_1.__extends(NotificationComponent, _super);
    function NotificationComponent(profileService) {
        var _this = _super.call(this) || this;
        _this.profileService = profileService;
        _this.monthSelector = MonthSelector;
        _this.listNofitication = [];
        return _this;
    }
    NotificationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.globalService.setBreadcrumbs([
            {
                label: 'Trang chủ',
                icon: 'pi pi-home',
                routerLink: '/'
            },
            {
                label: 'Trang cá nhân',
                routerLink: '/trang-ca-nhan',
                icon: 'pi pi-user',
            },
            {
                label: 'Thông báo',
                routerLink: '/trang-ca-nhan/thong-bao',
                icon: 'pi pi-th-large'
            }
        ]);
        var sub = this.profileService.getNotificationList()
            .subscribe(function (response) {
            _this.listNofitication = response.data.entries;
            _this.metaNotification = response.data.meta;
        });
        this.subscriptions.push(sub);
    };
    NotificationComponent = tslib_1.__decorate([
        Component({
            selector: 'app-profile-notification',
            templateUrl: './notification.component.html',
            styleUrls: ['./notification.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [ProfileService])
    ], NotificationComponent);
    return NotificationComponent;
}(BasePage));
export { NotificationComponent };
//# sourceMappingURL=notification.component.js.map