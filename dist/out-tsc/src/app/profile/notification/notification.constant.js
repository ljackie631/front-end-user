export var MonthSelector;
(function (MonthSelector) {
    MonthSelector.MONTHS = [
        {
            name: '1 tháng',
            value: 30
        },
        {
            name: '2 tháng',
            value: 60
        },
        {
            name: '3 tháng',
            value: 90
        }
    ];
})(MonthSelector || (MonthSelector = {}));
//# sourceMappingURL=notification.constant.js.map