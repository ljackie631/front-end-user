import { environment } from '../../../environments/environment';
export var API;
(function (API) {
    API.uploadImage = environment.serverImage + 'images';
    var Profile;
    (function (Profile) {
        Profile.AddressList = '';
    })(Profile = API.Profile || (API.Profile = {}));
    var Home;
    (function (Home) {
        Home.Get = environment.apiEndPoint + 'product/home';
    })(Home = API.Home || (API.Home = {}));
})(API || (API = {}));
//# sourceMappingURL=api.constant.js.map