import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
var GlobalService = /** @class */ (function () {
    function GlobalService() {
        this.breadcrumbs$ = new BehaviorSubject([]);
        this.loadingSpinner$ = new BehaviorSubject(false);
    }
    GlobalService.prototype.setLoadingSpinner = function (value) {
        this.loadingSpinner$.next(value);
    };
    GlobalService.prototype.getLoadingSpinner = function () {
        return this.loadingSpinner$.asObservable();
    };
    GlobalService.prototype.setBreadcrumbs = function (values) {
        var _this = this;
        setTimeout(function () {
            _this.breadcrumbs$.next(values);
        }, 100);
    };
    GlobalService.prototype.getBreadcrumbs = function () {
        return this.breadcrumbs$.asObservable();
    };
    GlobalService = tslib_1.__decorate([
        Injectable()
    ], GlobalService);
    return GlobalService;
}());
export { GlobalService };
//# sourceMappingURL=global.service.js.map