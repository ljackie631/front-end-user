import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ImageUploaderComponent } from './imageUploader.component';
import { StringService } from '../../services/string.service';
var ImageUploaderModule = /** @class */ (function () {
    function ImageUploaderModule() {
    }
    ImageUploaderModule = tslib_1.__decorate([
        NgModule({
            declarations: [ImageUploaderComponent],
            imports: [
                CommonModule,
                HttpClientModule
            ],
            exports: [
                ImageUploaderComponent
            ],
            providers: [
                StringService
            ]
        })
    ], ImageUploaderModule);
    return ImageUploaderModule;
}());
export { ImageUploaderModule };
//# sourceMappingURL=imageUploader.module.js.map