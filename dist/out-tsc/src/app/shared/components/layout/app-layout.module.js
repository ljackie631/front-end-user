import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InputTextareaModule, InputTextModule } from 'primeng/primeng';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { AppBrandBannerComponent } from './app-brand-banner/app-brand-banner.component';
import { AppFooterComponent } from './app-footer/app.footer.component';
import { AppTopBarComponent } from './app-topbar/app.topbar.component';
import { MenuComponent } from './menu/menu.component';
var AppLayoutModule = /** @class */ (function () {
    function AppLayoutModule() {
    }
    AppLayoutModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                AppTopBarComponent,
                AppFooterComponent,
                AppBrandBannerComponent,
                MenuComponent
            ],
            imports: [
                CommonModule,
                RouterModule,
                InputTextareaModule,
                InputTextModule,
                DynamicDialogModule,
                BrowserAnimationsModule
            ],
            exports: [
                AppTopBarComponent,
                AppFooterComponent,
                AppBrandBannerComponent,
                MenuComponent
            ]
        })
    ], AppLayoutModule);
    return AppLayoutModule;
}());
export { AppLayoutModule };
//# sourceMappingURL=app-layout.module.js.map