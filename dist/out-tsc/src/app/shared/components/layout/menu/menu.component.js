import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var MenuComponent = /** @class */ (function () {
    function MenuComponent() {
        this.menuItems = [
            { title: 'Hoa Sinh nhật', link: '' },
            { title: 'Hoa khai trương', link: '' },
            { title: 'Hoa tặng mẹ', link: '' },
            { title: 'Hoa cưới', link: '' },
            { title: 'Hoa tươi', link: '' },
            { title: 'Hoa tình yêu', link: '' },
            { title: 'Hoa cảm ơn', link: '' },
            { title: 'Hoa chia buồn', link: '' }
        ];
    }
    MenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-header-menu',
            templateUrl: './menu.component.html',
            styleUrls: [
                './menu.component.scss'
            ]
        })
    ], MenuComponent);
    return MenuComponent;
}());
export { MenuComponent };
//# sourceMappingURL=menu.component.js.map