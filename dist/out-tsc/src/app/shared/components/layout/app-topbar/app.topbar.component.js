import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { DialogService, MessageService } from "primeng/api";
import { PopupComponent } from "../../../../auth/popup/popup.component";
var AppTopBarComponent = /** @class */ (function () {
    function AppTopBarComponent(dialogService, messageService) {
        this.dialogService = dialogService;
        this.messageService = messageService;
    }
    AppTopBarComponent.prototype.show = function () {
        this.dialogService.open(PopupComponent, {
            header: "Flower Shop",
            width: "50%",
            contentStyle: { "max-height": "50%", overflow: "auto" }
        });
    };
    AppTopBarComponent = tslib_1.__decorate([
        Component({
            selector: "app-topbar",
            templateUrl: "./app.topbar.component.html",
            styleUrls: ["./app.topbar.component.scss"],
            providers: [DialogService, MessageService]
        }),
        tslib_1.__metadata("design:paramtypes", [DialogService,
            MessageService])
    ], AppTopBarComponent);
    return AppTopBarComponent;
}());
export { AppTopBarComponent };
//# sourceMappingURL=app.topbar.component.js.map