import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { GlobalService } from '../../services/global.service';
import { ServiceLocator } from '../../services/locator.service';
import { ValidatorService } from '../../services/validator.service';
var BasePage = /** @class */ (function () {
    function BasePage() {
        this.subscriptions = [];
        this.validatorService = ServiceLocator.injector.get(ValidatorService);
        this.authService = ServiceLocator.injector.get(AuthService);
        this.globalService = ServiceLocator.injector.get(GlobalService);
    }
    BasePage.prototype.getErrorMessages = function (form) {
        var _this = this;
        var controlNames = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            controlNames[_i - 1] = arguments[_i];
        }
        var errors = [];
        controlNames.forEach(function (name) {
            errors = errors.concat(_this.getErrorMessagesFromControlName(form, name));
        });
        return errors;
    };
    BasePage.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (subscription) {
            if (subscription) {
                subscription.unsubscribe();
            }
        });
    };
    BasePage.prototype.markAsTouchedForAll = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.markAsTouchedForAll(control);
            }
            else if (control instanceof FormArray) {
                control.controls.forEach(function (innerFormGroup) {
                    if (innerFormGroup instanceof FormGroup) {
                        _this.markAsTouchedForAll(innerFormGroup);
                    }
                });
            }
        });
    };
    BasePage.prototype.markAsUntouchedForAll = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsUntouched({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.markAsUntouchedForAll(control);
            }
            else if (control instanceof FormArray) {
                control.controls.forEach(function (innerFormGroup) {
                    if (innerFormGroup instanceof FormGroup) {
                        _this.markAsUntouchedForAll(innerFormGroup);
                    }
                });
            }
        });
    };
    BasePage.prototype.markAsPristineForAll = function (formGroup) {
        var _this = this;
        Object.keys(formGroup.controls).forEach(function (field) {
            var control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsPristine({ onlySelf: true });
            }
            else if (control instanceof FormGroup) {
                _this.markAsPristineForAll(control);
            }
            else if (control instanceof FormArray) {
                control.controls.forEach(function (innerFormGroup) {
                    if (innerFormGroup instanceof FormGroup) {
                        _this.markAsPristineForAll(innerFormGroup);
                    }
                });
            }
        });
    };
    BasePage.prototype.getErrorMessagesFromControlName = function (form, ctrlName) {
        var control = form.controls[ctrlName];
        return this.getSingleErrorMessages(control);
    };
    BasePage.prototype.getSingleErrorMessages = function (control) {
        var sErr = [];
        if (!control.valid && control.touched && control.errors) {
            sErr = Object.values(control.errors);
        }
        return sErr;
    };
    return BasePage;
}());
export { BasePage };
//# sourceMappingURL=base-page.js.map