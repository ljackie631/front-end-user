import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SearchBoxComponent } from './search-box.component';
import { DropdownModule, TabViewModule, ButtonModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
var SearchBoxModule = /** @class */ (function () {
    function SearchBoxModule() {
    }
    SearchBoxModule = tslib_1.__decorate([
        NgModule({
            declarations: [SearchBoxComponent],
            imports: [
                CommonModule,
                HttpClientModule,
                DropdownModule,
                FormsModule,
                TabViewModule,
                ReactiveFormsModule,
                ButtonModule
            ],
            exports: [
                SearchBoxComponent
            ],
            providers: []
        })
    ], SearchBoxModule);
    return SearchBoxModule;
}());
export { SearchBoxModule };
//# sourceMappingURL=search-box.module.js.map