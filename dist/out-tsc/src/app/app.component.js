import * as tslib_1 from "tslib";
import { Component, ElementRef, ViewChild } from '@angular/core';
import { distinctUntilChanged } from 'rxjs/operators';
import { BasePage } from './shared/components/base/base-page';
var AppComponent = /** @class */ (function (_super) {
    tslib_1.__extends(AppComponent, _super);
    function AppComponent() {
        var _this = _super.call(this) || this;
        _this.title = 'front-end-user';
        _this.breadcrumbItems = [];
        return _this;
    }
    AppComponent.prototype.onMobileMenuButton = function (event) {
        this.mobileMenuActive = !this.mobileMenuActive;
        event.preventDefault();
    };
    AppComponent.prototype.hideMobileMenu = function (event) {
        this.mobileMenuActive = false;
        event.preventDefault();
    };
    AppComponent.prototype.ngOnInit = function () {
        this.renderBreadcrumb();
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.globalService.getLoadingSpinner()
            .subscribe(function (value) {
            if (value) {
                _this.spinner.nativeElement.style.display = 'flex';
            }
            else {
                _this.spinner.nativeElement.style.display = 'none';
            }
        });
    };
    AppComponent.prototype.renderBreadcrumb = function () {
        var _this = this;
        this.globalService.getBreadcrumbs()
            .pipe(distinctUntilChanged())
            .subscribe(function (items) {
            _this.breadcrumbItems = items;
        });
    };
    tslib_1.__decorate([
        ViewChild('spinner'),
        tslib_1.__metadata("design:type", ElementRef)
    ], AppComponent.prototype, "spinner", void 0);
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}(BasePage));
export { AppComponent };
//# sourceMappingURL=app.component.js.map