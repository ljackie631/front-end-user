import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent = tslib_1.__decorate([
        Component({
            selector: 'app-home-page',
            templateUrl: './home.component.html',
            styleUrls: ['./home.component.scss']
        })
    ], HomeComponent);
    return HomeComponent;
}());
export { HomeComponent };
//# sourceMappingURL=home.component.js.map