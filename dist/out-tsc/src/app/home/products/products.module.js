import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductsPromotionComponent } from './products-promotion/products-promotion.component';
import { ProductItemModule } from '../../shared/components/product-item/product-item.module';
import { ProductsFeaturedComponent } from './products-featured/products-featured.component';
var ProductsModule = /** @class */ (function () {
    function ProductsModule() {
    }
    ProductsModule = tslib_1.__decorate([
        NgModule({
            declarations: [ProductsPromotionComponent, ProductsFeaturedComponent],
            imports: [
                CommonModule,
                ReactiveFormsModule,
                ProductItemModule
            ],
            exports: [
                ProductsPromotionComponent,
                ProductsFeaturedComponent
            ],
            providers: []
        })
    ], ProductsModule);
    return ProductsModule;
}());
export { ProductsModule };
//# sourceMappingURL=products.module.js.map