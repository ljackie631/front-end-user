import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DetailProductComponent } from './detail-product.component';
import { ProductItemModule } from '../shared/components/product-item/product-item.module';
import { SearchBoxModule } from '../shared/components/search-box/search-box.module';
import { GalleriaModule } from 'primeng/galleria';
import { SpinnerModule, ButtonModule } from 'primeng/primeng';
import { FormsModule } from '@angular/forms';
import { ProductsSimilarComponent } from './products-similar/products-similar.component';
var DetailProductModule = /** @class */ (function () {
    function DetailProductModule() {
    }
    DetailProductModule = tslib_1.__decorate([
        NgModule({
            declarations: [DetailProductComponent, ProductsSimilarComponent],
            imports: [
                CommonModule,
                FormsModule,
                ProductItemModule,
                SearchBoxModule,
                GalleriaModule,
                SpinnerModule,
                ButtonModule
            ],
            exports: [DetailProductComponent]
        })
    ], DetailProductModule);
    return DetailProductModule;
}());
export { DetailProductModule };
//# sourceMappingURL=detail-product.module.js.map