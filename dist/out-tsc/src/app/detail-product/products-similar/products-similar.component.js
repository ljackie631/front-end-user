import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { Products } from '../../shared/constants/products.constant';
var ProductsSimilarComponent = /** @class */ (function () {
    function ProductsSimilarComponent() {
    }
    ProductsSimilarComponent.prototype.ngOnInit = function () {
        this.products = Products;
    };
    ProductsSimilarComponent = tslib_1.__decorate([
        Component({
            selector: "app-products-similar",
            templateUrl: "./products-similar.component.html",
            styleUrls: ["./products-similar.component.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], ProductsSimilarComponent);
    return ProductsSimilarComponent;
}());
export { ProductsSimilarComponent };
//# sourceMappingURL=products-similar.component.js.map