import * as tslib_1 from "tslib";
import { Component, HostListener } from "@angular/core";
var PRODUCT_DETAIL = {
    title: "Hoa tặng Sinh Nhật - Tình bằng hữu",
    images: [
        "/assets/demo/lily.jpg",
        "/assets/demo/img-1.jpg",
        "/assets/demo/img-2.jpg",
        "/assets/demo/img-3.jpg",
        "/assets/demo/img-4.jpg",
        "/assets/demo/img-5.jpg"
    ],
    sku: "FS123456",
    description: "Something text about Description!",
    topic: 5,
    priceRange: 2,
    slug: "hoa-tinh-bang-huu",
    originalPrice: 500000,
    saleOff: {
        price: 300000,
        startDate: new Date(),
        endDate: new Date(),
        active: true
    },
    code: "FS",
    updatedAt: new Date(),
    createdAt: new Date()
};
var DetailProductComponent = /** @class */ (function () {
    function DetailProductComponent() {
        this.product = PRODUCT_DETAIL;
        this.discount = 100 - this.product.saleOff.price * 100 / this.product.originalPrice;
    }
    DetailProductComponent.prototype.onResize = function (event) {
        this.width = event.target.innerWidth;
    };
    DetailProductComponent.prototype.ngOnInit = function () {
        this.initGalleria();
    };
    DetailProductComponent.prototype.initGalleria = function () {
        this.images = [];
        for (var i = 0; i < this.product.images.length; i++) {
            this.images.push({ source: this.product.images[i] });
        }
    };
    tslib_1.__decorate([
        HostListener('window:resize', ['$event']),
        tslib_1.__metadata("design:type", Function),
        tslib_1.__metadata("design:paramtypes", [Object]),
        tslib_1.__metadata("design:returntype", void 0)
    ], DetailProductComponent.prototype, "onResize", null);
    DetailProductComponent = tslib_1.__decorate([
        Component({
            selector: "app-detail-product",
            templateUrl: "./detail-product.component.html",
            styleUrls: ["./detail-product.component.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], DetailProductComponent);
    return DetailProductComponent;
}());
export { DetailProductComponent };
//# sourceMappingURL=detail-product.component.js.map