import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { DetailProductComponent } from './detail-product/detail-product.component';
var routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent
    },
    {
        path: 'ket-qua-tim-kiem',
        component: SearchResultsComponent
    },
    {
        path: 'detail-product',
        component: DetailProductComponent
    },
    {
        path: 'demo',
        loadChildren: './demo/demo.module#DemoModule'
    },
    {
        path: 'trang-ca-nhan',
        loadChildren: './profile/profile.module#ProfileModule'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map