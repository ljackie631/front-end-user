import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { BasePage } from '../../shared/components/base/base-page';
var DemoFormComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DemoFormComponent, _super);
    function DemoFormComponent(fb, messageService) {
        var _this = _super.call(this) || this;
        _this.fb = fb;
        _this.messageService = messageService;
        return _this;
    }
    Object.defineProperty(DemoFormComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.form.value);
        },
        enumerable: true,
        configurable: true
    });
    DemoFormComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            firstname: ['', this.validatorService.checkRequired()],
            lastname: ['', this.validatorService.checkRequired()],
            password: ['', Validators.compose([
                    this.validatorService.checkRequired(),
                    this.validatorService.checkMinLength(6)
                ])],
            description: [''],
            gender: ['', this.validatorService.checkRequired()],
            images: [[
                    {
                        link: 'http://157.230.248.161:3100/images/temps/demo/2019/04/23/20190423100133_10begjb5jutmdlti',
                        isDemo: false
                    }
                ]]
        });
        this.genders = [];
        this.genders.push({ label: 'Select Gender', value: '' });
        this.genders.push({ label: 'Male', value: 'Male' });
        this.genders.push({ label: 'Female', value: 'Female' });
    };
    DemoFormComponent.prototype.onSubmit = function (value) {
        this.submitted = true;
        this.messageService.add({ severity: 'info', summary: 'Success', detail: 'Form Submitted' });
    };
    DemoFormComponent.prototype.onChangeLink = function (value) {
        console.log('link image upload', value);
    };
    DemoFormComponent = tslib_1.__decorate([
        Component({
            selector: 'app-demo-form',
            templateUrl: './demo-form.component.html',
            styleUrls: ['./demo-form.component.scss'],
            providers: [MessageService]
        }),
        tslib_1.__metadata("design:paramtypes", [FormBuilder, MessageService])
    ], DemoFormComponent);
    return DemoFormComponent;
}(BasePage));
export { DemoFormComponent };
//# sourceMappingURL=demo-form.component.js.map