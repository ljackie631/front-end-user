var PostItem = /** @class */ (function () {
    function PostItem(component, data) {
        this.component = component;
        this.data = data;
    }
    return PostItem;
}());
export { PostItem };
//# sourceMappingURL=post-item.js.map