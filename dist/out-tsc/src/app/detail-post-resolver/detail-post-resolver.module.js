import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { DetailPostResolverComponent } from './detail-post-resolver.component';
import { PostDirective } from './post.directive';
var DetailPostResolverModule = /** @class */ (function () {
    function DetailPostResolverModule() {
    }
    DetailPostResolverModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                DetailPostResolverComponent,
                PostDirective
            ],
            imports: [
                CommonModule,
                RouterModule,
                BrowserModule
            ],
            exports: [DetailPostResolverComponent]
        })
    ], DetailPostResolverModule);
    return DetailPostResolverModule;
}());
export { DetailPostResolverModule };
//# sourceMappingURL=detail-post-resolver.module.js.map